# Pooling Experiments

This repository houses a few experiments involvign alternative pooling strategies for convolutional neural networks (CNNs).

These codes require [MatConvNet](http://www.vlfeat.org/matconvnet/) version 1.0 beta 18 or better.

## Quick Start

### Testing the Discrete Centered Maximal Pooling (CMF) implementation.

1.  Edit the setup.m script in the root of this project directory so that it references your local install of matconvnet.

2.  Navigate into src/thirdparty and download all the third party scripts described in that [README](./src/thirdparty/README.md)

3.  From within matlab, navagiate to the root project directory and run setup.m.

4.  Navigate into src/pooling and run the test_cmf_pool.m script. The result should be an example visualzation of different pooling strategies on the canonical cameraman image.



### Testing Different Pooling Strategies with CIFAR-10

1.  Run the setup steps 1-3 from the previous section (if you haven't already).

2.  Edit the examples/cifar_10/cnn_cifar_init.m to select the pooling strategy you would like to use.  To use CMF pooling, enable the corresponding line as shown below:

        % mjp: choose what function to use to
        %      implement average pooling.
        %poolType = 'pool';
        poolType = 'cmf-pool-umd';
You can also change other network properties as desired.  For example, to try the "avg-avg-avg" pooling layer configuration, change the "method" parameter in the first pooling layer.

3. Run the train_cifar.m script.  Note that this will take awhile
   (several hours in the case of CMF pooling).

### Testing Different Pooling Strategies with Caltech 101

TODO: coming soon
