In \citep{boureau2010theoretical} the authors provide one of the first attempts to theoretically characterize the action of pooling functions.
Their insight was to focus on the binary classification setting, where the difficulty of the problem is related to the degree of overlap between the distribution of features from each class.
%
Hence, a natural metric in this context is some measure of how pooling impacts the separability of these distributions.
%
Below we provide a brief review of this framework.

\TODO{Some comment about the coding-pooling framework that used to be prevalent prior to CNNs; still a reasonable framework for theoretical discussion}

Of interest are \emph{mid-level features} for object recognition, informally defined as global features created by transforming and combining lower-level features extracted from the input image.
The notion of a mid-level feature is quite general; examples include bags of features, spatial pyramids and the upper units of convolutional neural networks.
Of particular interest is a specific class of mid-level feature obtained by first applying a \emph{coding} algorithm to the low-level features and then aggregating these encoded features via \emph{spatial pooling}.
%
The coding transformation produces a signal representation with desirable properties, such as sparsity or statistical independence; the specific choice of coding algorithm will inform the statistical model used to represent the feature distribution.
The analysis will then focus upon how different pooling algorithms interact with these distributions.



More concretely, let an image $I$ be represented by a set of low-level input descriptors $\{\vx_i\}_{i=1}^N$ where each column vector $\vx_i \in \mathbb{R}^D$ has been computed at one of $N$ unique locations within the image.  
For example, $\vx_i$ might correspond to SIFT descriptors extracted at
some pixel location.
One then defines $M$ different \emph{regions of interest} (or \emph{pooling regions}) $\{ \mathcal{N}_m \}_{m=1}^M$ where each set $\mathcal{N}_m$ contains the locations/indices associated with pooling region $m$.
For example, \emph{whole image pooling} has $M=1$ and $\mathcal{N}_1 = \{1, \ldots, N\}$.   
In the case of gridded pooling (typical in convolutional neural networks) the $\mathcal{N}_m$ induce a partition on the set of pixel locations.
A three-level spatial pooling pyramid might have $M=21 = 16 + 4 +1$ different (hierarchical) pooling regions.

Given the pooling regions and input descriptors, the coding and pooling operators $f$ and $g$ are applied sequentially and the outputs concatenated to form a vector $\vz$ which constitutes the whole image representation
\begin{align*}
  \valpha_i & = f(\vx_i),  & i = 1, \ldots, N \\ 
  \vh_m & = g( \{ \valpha_i \}_{i \in \mathcal{N}_m}),  &  m = 1, \ldots, M \\
  \vz^T &= [\vh_1^T \ldots \vh^T_M].
\end{align*}
%
Here, coded features $\valpha_i \in \mathbb{R}^K$ or $\valpha_i \in \{0,1\}^K$  (where $K$ denotes the codebook cardinality) depending upon the coding algorithm employed\footnote{In \citep{boureau2010learning} $D=128$ (SIFT default) and the dictionary sizes are one of $K \in \{256, 512, 1024\}$.}.  
%
%
The pooled outputs $\vh_m \in \mathbb{R}^K$ can be produced by any number of pooling operators, such as average or maximum pooling.
%\begin{align*}
%\vh_m^{avg} & = \frac{1}{|\mathcal{N}_m|} \sum_{i \in \mathcal{N}_m} \valpha_i ,  &
%\vh_{m,j}^{max} &= \underset{i \in \mathcal{N}_m}{\max \;} \valpha_{i,j},  \quad   j = 1, \ldots, K.
%\end{align*}

The statistical model proposed by \citep{boureau2010theoretical} focuses upon the contribution of a single codebook feature.
Consider the unpooled data for some region of interest $\mathcal{N}_m$ with $\left| \mathcal{N}_m \right| =P$.
These features can be represented by a $P \times K$ matrix with rows $\{ \valpha_i \}_{i \in \mathcal{N}_m}$; an arbitrary column $\vv$ of this matrix indicates the strength of the corresponding feature at each of the $P$ locations within the pooling region.
For a 1-of-$K$ code (such as might be obtained by vector quantization) $\vv$ consists of 0s and 1s indicating the absence or presence of the $k$th feature at each location.
Alternatively, if a sparse coding algorithm is used, $\vv$ is real and sparse.
%
In the case of 1-of-$K$ codes, the authors elect to model the $P$ components of $\vv$ as i.i.d. Bernoulli random variables; for sparse coding, they suggest i.i.d. exponential or Laplace random variables.
%
In either case, they acknowledge that the independence assumption is usually unjustified since spatially proximate features are usually highly correlated; however, this assumption greatly facilitates mathematical analysis.
%
For a single vector $\vv$ the average and maximum pooling have simple definitions $g_a(\vv) = P^{-1} \sum_{i=1}^P \vv_i$ and $g_m(\vv) = \max_i \vv_i$.

Given two classes $C_1$ and $C_2$, the goal is to study the overlap of the conditional distributions $p(g \mid C_1)$ and $p(g \mid C_2)$ for various pooling functions $g$.  
%
Intuitively, if one considers only the first two moments, one metric is to consider the difference in means of these two distributions relative to the standard deviations; \citep{boureau2010theoretical} introduce the notation
\[
\psi := \frac{|\E(g \mid C_1) - \E(g \mid C_2) |} { \sigma_1 + \sigma_2}  ,
\]
to denote this ratio.

\TODO{run this part down properly}
I'm surprised \citep{boureau2010theoretical} doesn't mention LDA; \citep{li2015beyond} does. In linear discriminant analysis (LDA) a measure of signal-to-noise ratio for class labeling is given by
\[
S := \frac{\sigma^2_{between}}{ \sigma^2_{within}} .
\]
Note that I believe LDA makes some assumptions about normality.
LDA seeks a linear vector $\vw$ that maximizes separability; 
it can be shown that the maximum separation occurs when $\vw \propto (\mu_1 - \mu_2) / (\sigma_1 + \sigma_2)$.

\TODO{Also, we could consider other metrics (e.g. KL divergence?)}
(an implicit assumption is that $\E(g \mid C_1)$ and $\E(g \mid C_2)$ follow the same distribution but use different parameters, and hence have different means).
%
For binary features, sums of i.i.d. Bernoulli variables have a binomial distribution and the maximum of i.i.d. Bernoulli random variables can be shown to follow a different Bernoulli distribution; these observations permit the authors to derive analytical expressions for $\psi$ for both average and maximum pooling. 
%
In the case of sparse features, analogous expressions can be derived from the c.d.f. of the exponential distribution.  For full details, the reader is referred to \citep{boureau2010theoretical}.


Note that \citep{li2015beyond} employ a similar framework; they extend to a multi-variate approach that relaxes the i.i.d. assumption upon feature values.
However, this approach seems to focus almost exclusively on variance, may neglect some corner cases (such as trivial pooling functions), does not attempt to justify the Gaussian feature assumption and does not provide a comparative analysis of other types of pooling functions nor an in-depth investigation into the overlapping pooling case.  
