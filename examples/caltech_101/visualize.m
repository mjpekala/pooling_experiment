% Visualize elements of caltech 101 and pooling actions.
% Before running this script, download and unzip the caltech 101 data set.
%

% mjp, April 2016

%% Parameters
rMax = 5;        % the pooling size to use
channel = 1;     % the channel to visualize


%% Load data
if ~exist('vl_nnpool')
    error('cannot find vl_nnpool; did you forget to run the setup() script?');
end

[fn, path] = uigetfile('*.jpg');
fn = fullfile(path, fn);

%--------------------------------------------------
% Visualize action of maximum pooling filter (forward pass)
%--------------------------------------------------
X = double(imread(fn));

Xavg = vl_nnpool(X, 2*rMax+1, 'stride', 2*rMax+1, 'method', 'avg');
Xmax = vl_nnpool(X, 2*rMax+1, 'stride', 2*rMax+1, 'method', 'max');
[Xcmf,Rcmf] = cmf_pool_vl(X, rMax);

% It should always be the case that:
%   maximum pooling >= maxfun pooling
%   maxfun pooling >= average pooling
%
% Verify this property holds here.
assert(all(Xcmf(:) <= Xmax(:)));
assert(all(Xavg(:) <= Xcmf(:)));


figure('Position', [100 100 1200 300]);
ha = tight_subplot(1, 4, [.05 .05], .08, .08);

axes(ha(1));
imagesc(X(:,:,channel), [0 255]);  colorbar();
title(sprintf('input image, channel %d', channel));

axes(ha(2));
imagesc(Xavg(:,:,channel), [0 255]);  colorbar();
title(sprintf('average pooling, r=%d', rMax));

axes(ha(3));
imagesc(Xmax(:,:,channel), [0 255]);  colorbar();
title(sprintf('max pooling, r=%d', rMax));

axes(ha(4));
imagesc(Xcmf(:,:,channel), [0 255]); colorbar();
title(sprintf('CMF pooling, rMax=%d', rMax));

