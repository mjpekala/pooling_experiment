# CIFAR-10 Pooling Experiment

## Quick Start

Note: If you run the train/deploy from within this directory
(**required**) then Matlab will pick up the local copy of *vl_simplenn()* in place of the version that is included with MatConvNet.  This is needed in order to pick up our new pooling functions.

The first step in using this code is to modify setup.m to point to your locally installed version of MatConvNet.


Then, launch Matlab and navigate into this directory and run
```
    train_cifar();
```

Once training is complete, you can also run in "deploy" mode (re-runs the pre-trained network on a subset of CIFAR-10).  In *deploy_cifar.m* file, update the p_.epoch variable (if necessary) and then run

```
    deploy_cifar();
```		

The network outputs (from each layer) are available in the variable called "result".  You can query these interactively within the main processing loop or, after deployment is complete, examine the results from the final mini-batch.  For example, you can observe that the output of the first layer of the CNN consists of 100 different 32x32 images, each having three channels (red, green, blue):

```
    >> size(result(1).x)
    ans =  32    32     3   100
```

The second-to-last layer contains the estimates for each class (there are 10 classes in this dataset):
```
    >> size(result(end-1).x)
    ans = 1     1    10   100
```


## Alternative Pooling Implementations

Thel *pool_mf.m* file is a place where we can start experimenting with implementations of an alternative pooling function.  You'll see where it is called in vl_simplenn().  We can also change the code in vl_simplenn() (e.g. eliminate the unnecessary parameters in the call to pool_mf()) once we have a candidate implementation.
