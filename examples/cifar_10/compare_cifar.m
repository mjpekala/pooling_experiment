% COMPARE_CIFAR  Compare performance of various pooling methods on
%                CIFAR-10 data set.

% mjp, april 2015


%% Parameters
cifarDir = '/Users/pekalmj1/Apps/matconvnet-1.0-beta18/data/cifar-lenet';
maxNetDir = fullfile(cifarDir, 'Downsample3Layer1', 'max');
avgNetDir = fullfile(cifarDir, 'Downsample3Layer1', 'avg');
cmfNetDir = fullfile(cifarDir, 'Downsample3Layer1', 'cmf');
epoch = 45;


%% Load data/networks
fprintf('[%s]: loading CIFAR images from "%s" ...\n', mfilename, cifarDir);
imdb = load(fullfile(cifarDir, 'imdb.mat'));

% determine test data
idx = (imdb.images.set == 3);
test.X = imdb.images.data(:,:,:,idx);
test.y = imdb.images.labels(idx);

% Load previously saved model.
fprintf('[%s]: loading CNNs\n', mfilename);
netFn = ['net-epoch-' num2str(epoch) '.mat'];
maxNet = load(fullfile(maxNetDir, netFn), 'net', 'info');
avgNet = load(fullfile(avgNetDir, netFn), 'net', 'info');
cmfNet = load(fullfile(cmfNetDir, netFn), 'net', 'info');


%% Deploy models
fprintf('[%s]: deploying, pool layer 1 is "max"...\n', mfilename);
yHatMax = deploy_cifar(maxNet.net, test.X, test.y, imdb.meta.classes);
fprintf('[%s]: deploying, pool layer 1 is "avg"...\n', mfilename);
yHatAvg = deploy_cifar(avgNet.net, test.X, test.y, imdb.meta.classes);
fprintf('[%s]: deploying, pool layer 1 is "cmf"...\n', mfilename);
yHatCmf = deploy_cifar(cmfNet.net, test.X, test.y, imdb.meta.classes);


%%% Compare performance

% Load the original CIFAR data (easier to visualize)
cifar = load('cifar-10-batches-mat/test_batch.mat');
cifar.labels = cifar.labels + 1;            % preprocessing done by matconvnet
assert(all(cifar.labels(:) == test.y(:)));  % make sure we have the right subset of CIFAR-10


%% Compare max and average pooling
disagree.idx = find(yHatMax ~= yHatAvg);

% It seems matconvnet will be unhappy with a data set size that is not a multiple of the batch size.
bs = maxNet.net.meta.trainOpts.batchSize;   
n = floor(length(disagree.idx) / bs);
disagree.idx = disagree.idx(1:n*bs);

disagree.X = test.X(:,:,:,disagree.idx);
disagree.y = test.y(disagree.idx);


[cmp.yHatMax, cmp.filtMax] = deploy_cifar(maxNet.net, disagree.X, disagree.y, imdb.meta.classes);
[cmp.yHatAvg, cmp.filtAvg] = deploy_cifar(avgNet.net, disagree.X, disagree.y, imdb.meta.classes);


outDir = 'Disagree_MAX_vs_AVG';
if ~exist(outDir, 'dir')
    mkdir(outDir);
end

%for ii = 1:numel(disagree.y)
for ii = 1:200
    fig = figure('Position', [100 100 900 700]);
    ha = tight_subplot(6, 1, [.01, .01], [.01 .05], .01);
    
    axes(ha(1));
    img = cifar.data(disagree.idx(ii),:);
    img = reshape(img, [32 32 3]);
    img = permute(img, [2 1 3]);
    imshow(img)
    ylabel('raw image');
    
    title(sprintf('CIFAR-10 test image # %d; y=%s, y-max=%s, y-avg=%s', ...
                  disagree.idx(ii), ...
                  imdb.meta.classes{test.y(disagree.idx(ii))}, ...
                  imdb.meta.classes{cmp.yHatMax(ii)}, ...
                  imdb.meta.classes{cmp.yHatAvg(ii)}));
    
    axes(ha(2));
    img = cmp.filtMax.input(:,:,:,ii);
    %imshow(reshape(img, size(img,1), size(img,2)*size(img,3)), [min(img(:)) max(img(:))]);
    plot_filter_tensor_2(img, 1);
    colorbar;
    ylabel('cnn input');
 
    axes(ha(3));
    img = cmp.filtMax.conv1(:,:,:,ii);
    plot_filter_tensor_2(img, 2);
    colorbar;
    ylabel('conv1 (max)');
    
    axes(ha(4));
    img = cmp.filtMax.conv1(:,:,:,ii);
    plot_filter_tensor_2(max(0,img), 2);
    colorbar;
    ylabel('RELU (max)');
    
    axes(ha(5));
    img = cmp.filtAvg.conv1(:,:,:,ii);
    plot_filter_tensor_2(img, 2);
    colorbar;
    ylabel('conv1 (avg)');
    
    axes(ha(6));
    img = cmp.filtAvg.conv1(:,:,:,ii);
    plot_filter_tensor_2(max(0,img), 2);
    colorbar;
    ylabel('RELU (avg)');

    fig.PaperUnits = 'inches';
    fig.PaperPosition = [0 0 11 8];
    print(fullfile(outDir, sprintf('max_vs_avg_%04d.png', ii)), '-dpng'); %, '-r0');
    close;
end

