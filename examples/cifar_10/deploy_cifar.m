function [yHat, filters] = deploy_cifar(net, X, y, classNames)
% DEPLOY_CIFAR  Runs a (previously trained) CNN on validation data.
%

yHat = zeros(size(y));

bs = net.meta.trainOpts.batchSize;   % bs := batch size
filters.input = {};
filters.conv1 = {};

tic
for a = 1:bs:length(y)
    b = min(a+bs-1, length(y));
    result = vl_simplenn(net, X(:,:,:,a:b));
    n = length(result);                 % n: number of layers
    est = squeeze(result(n-1).x);       % XXX: what are these values?  not probabilities...
    [~, yHat(a:b)] = max(est, [], 1);   % take the argmax to get class estimate

    % store intermediate filter results (optional)
    if nargout >= 2
        filters.input = [filters.input result(1).x];
        filters.conv1 = [filters.conv1 result(2).x];
    end
    
end
fprintf('[%s]: CNN deploy time: %0.2f (sec)\n', mfilename, toc);


if nargout >= 2
    filters.input = cat(4, filters.input{:});
    filters.conv1 = cat(4, filters.conv1{:});
end


%% Analysis

acc = sum(y == yHat) / numel(y);
fprintf('[%s]: accuracy on test data is %0.2f%%\n', mfilename, 100*acc);

yAll = sort(unique(y));
accPerClass = zeros(size(yAll));
for ii = 1:numel(accPerClass), yi = yAll(ii);
    accPerClass(ii) =  sum(y == yHat & y == yi) / sum(y == yi);
end

C =  confusionmat(double(y), yHat);
for ii = 1:size(C,1)
    fprintf('%10s : ', classNames{yAll(ii)});
    for jj = 1:size(C,2)
        fprintf('%4d', C(ii,jj));
    end
    fprintf('  |  %0.2f%%\n', 100*accPerClass(ii));
end

y = y(:);
yHat = yHat(:);
