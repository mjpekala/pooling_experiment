function plot_filter_tensor(X, layout, sameScale)
% PLOT_FILTER_TENSOR Displays a set of 2D filters.
%
%      plot_filter_tensor(X)
%
%    where,
%        X is a tensor of filters with dimension (width x height x nFilters)
%
%    Example:
%      If X is a tensor with dimensions:
%         (nInputFilt, nOutputFilt, width, height)
%      as might be in the case of a Caffe convolutional
%      filter layer, one could do:
%
%         plot_filter_tensor(permute(squeeze(X(1,:,:,:)), [2 3 1]));

% July 2015, mjp

if nargin < 3, sameScale = 1; end
if nargin < 2, layout = [3]; end

[w,h,n] = size(X);
xMax = max(X(:));  xMin = min(X(:));

%--------------------------------------------------
% Infer layout of filters (in case layout doesn't account 
% for all filters already)
%--------------------------------------------------

nRemaining = size(X,3) - sum(layout);
if nRemaining > 0
    nPerRow = ceil(sqrt(nRemaining));
    for ii = 1:nPerRow
        layout = [layout ; min(nPerRow, nRemaining)];
        nRemaining = nRemaining - nPerRow;
    end
end

M = zeros(length(layout), max(layout));
idx = 1;
for ii = 1:length(layout)
    nThisRow = layout(ii);
    M(ii, 1:nThisRow) = idx:idx+nThisRow-1;
    idx = idx + nThisRow;
end



%--------------------------------------------------
% Render
%--------------------------------------------------
figure('Position', [200 300 120*size(M,2), 120*size(M,1)]); 
ha = tight_subplot(size(M,1), size(M,2), [.03, .01]);

idx = 1;
for ii = 1:size(M,1)
    for jj = 1:size(M,2)
        axes(ha(idx)); idx = idx + 1;
        
        if M(ii,jj) > 0
            Xi = X(:,:,M(ii,jj));   
            
            normF = norm(Xi,'fro');
            if sameScale
                imagesc(Xi, [xMin, xMax]);  
            else
                imagesc(Xi);
            end
            
            set(gca, 'Xtick', [], 'Ytick', []);
            title(sprintf('(||x||_F=%0.3f)', normF));
        else
            set(gca, 'visible', 'off');
        end
    end
end


axes(ha(1));
suptitle(sprintf('min/max: %0.3f, %0.3f', xMin, xMax));
colormap('bone');

