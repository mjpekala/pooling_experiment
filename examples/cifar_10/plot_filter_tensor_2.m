function plot_filter_tensor_2(X, nRows)
%  X : an image tensor with dimensions (H, W, #_images)

if nargin < 2, asRow = 0; end

assert(ndims(X) == 3)
sz = size(X);

nCols = ceil(sz(3) / nRows);

Xr = zeros(1 + (sz(1)+1)*nRows, 1 + (sz(2)+1)*nCols);

idx = 1;
for ii = 1:nRows
    for jj = 1:nCols
        if idx <= sz(3)
            a = 2 + (ii-1)*(sz(1)+1);
            b = 2 + (jj-1)*(sz(2)+1);
            Xr(a:a+sz(1)-1, b:b+sz(2)-1) = X(:,:,idx);
            idx = idx + 1;
        end
    end
end

imshow(Xr, [min(Xr(:)), max(Xr(:))]);
colormap('bone');


% draw grid lines
hold on
for ii=1:sz(1)+1:size(Xr,1)
    line([0 size(Xr,2)], [ii ii], 'Color', 'r');
end
for ii=1:sz(2)+1:size(Xr,2)
    line([ii ii], [0 size(Xr,1)], 'Color', 'r');
end
hold off;

set(gca, 'XTick', [], 'YTick', []);


