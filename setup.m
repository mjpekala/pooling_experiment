function mcnPath = setup()
% SETUP  Sets up path to MatConvNet
%
%   ** 
%   Modify the mcnPath as needed for your system's local install of
%   MatConvNet. 
%   **

here = fileparts(mfilename('fullpath'));

% make sure matconvnet can be found in search path
if exist('vl_nnpool') ~= 3
    mcnPath = fullfile('/Users', 'pekalmj1', 'Apps', 'matconvnet-1.0-beta18');
    run(fullfile(mcnPath, 'matlab', 'vl_setupnn.m'));
    fprintf('[%s]: using MatConvNet in "%s"\n', mfilename, mcnPath);
end

% add in our custom pooling codes.
addpath(fullfile(here, 'src', 'pooling'));
addpath(fullfile(here, 'src', 'thirdparty'));

