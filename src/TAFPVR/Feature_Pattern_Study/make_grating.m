function X = make_grating(gap, n)

if nargin < 3, n = 128; end

nLines = 10;
lineLength = 20;

X = zeros(n,n);

xMax = size(X,2) - nLines * (gap+1) - 5;
yMax = size(X,1) - lineLength - 5;

x0 = ceil(rand()*xMax);
y0 = ceil(rand()*yMax);

for ii = 1:nLines
    X(y0:y0+lineLength, x0+(ii-1)*gap) = 1;
end
