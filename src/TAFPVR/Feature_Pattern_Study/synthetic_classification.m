% SYNTHETIC_CLASSIFICATION  A simple classification problem using
%                           synthetic signals.
%
%


% TODO: some localized high-frequency content that MAXFUN can hone in
% on but would be lost in the noise with a global pooling approach?  Will such a localized oscillation be preserved by sparse coding?

% mjp, may 2015

rng(9999)


%% Parameters
mu = [8 2]';
sigma = [2 2]';

nExamples = 2e2;              % total # number of examples 
nTrain = 15;                  % # training examples per class
sz = 200;                     % width/height of image objects
nAtoms = 256;                 % number of columns in dictionary


run_sift = @(I) sift_macrofeatures(I, 'subsamp', 1, 'step', 4, 'macrosl', 1);


%% Create objects

y = 1+mod(0:nExamples-1,2)';              % true class label
k = mu(y) + sigma(y) .* randn(size(y));   % object parameters
%k = grateGap(y);

figure; boxplot(k, y, 'labels', {'class 1', 'class 2'});
ylabel('wavenumber');
title('binary classification problem');

%Img = cellfun(@(x) real(make_grating(x, sz)), ...
%            num2cell(k), 'UniformOutput', 0);
Img = cellfun(@(x) real(make_plane_wave(x, x, sz)), ...
            num2cell(k), 'UniformOutput', 0);



%% Images -> SIFT descriptors
fprintf('[%s]: running SIFT on %d objects, please wait...\n', mfilename, length(Img));
siftTimer = tic;
X = cellfun(@(x) run_sift(single(x)), Img, 'UniformOutput', 0);
X = cat(4, X{:});
X = double(X);
fprintf('[%s]: SIFT took %0.2f (sec)\n', mfilename, toc(siftTimer));


%% Learn dictionary

% take a tensor with dimensions:   (rows, cols, #features, #examples)
% and reshapes into a matrix:      (#features, #objects)
tensor_to_matrix = @(T) reshape(permute(T, [3 1 2 4]), size(T,3), size(T,1)*size(T,2)*size(T,4));

isTrain = select_training(y, nTrain);

dlTimer = tic;
[D, param] = learn_dictionary(tensor_to_matrix(X(:,:,:,isTrain)), y, ...
                              'k', 2, 'lambdas', [.0001], 'nAtoms', nAtoms);
fprintf('[%s]: dictionary learning took %0.2f (sec)\n', mfilename, toc(dlTimer));


%% Sparse coding
fprintf('[%s]: encoding data using dictionary\n', mfilename);
    
scTimer = tic;

Xsc = zeros(size(X,1), size(X,2), size(D,2), size(X,4));
for ii = 1:size(X, 4)
    Xi = X(:,:,:,ii);
    [height, width, chan] = size(Xi);
    Xi = reshape(Xi, height*width, chan);
    Ai = full(mexLasso(Xi', D, param));
    Ai = reshape(Ai', height, width, size(D,2));
    Xsc(:,:,:,ii) = Ai;
end
fprintf('[%s]: sparse coding (train data) took %0.2f (sec)\n', mfilename, toc(scTimer));


for objIdx = [1 4]
    view_sift_features(X(:,:,:,objIdx), 2, 2, Img{objIdx});
    view_features(Xsc(:,:,:,objIdx));
end



%% Pooling
poolTimer = tic;

avg.Xtrain = spatial_pool(Xsc(:,:,:,isTrain), 'avg');
avg.Xtest = spatial_pool(Xsc(:,:,:,~isTrain), 'avg');

% need to pick a hyper-parameter for MAXFUN pooling
wi_mf_pool = @(X, k) spatial_pool(X, 'fun', k);
pMF = select_scalar_pooling_param(Xsc(:,:,:,isTrain), y(isTrain), wi_mf_pool, 4:2:20);
    
fun.Xtrain = spatial_pool(Xsc(:,:,:,isTrain), 'fun', pMF);
fun.Xtest = spatial_pool(Xsc(:,:,:,~isTrain), 'fun', pMF);

fprintf('[%s]: pooling took %0.2f (sec)\n', mfilename, toc(poolTimer));

figure; boxplot(avg.Xtrain, y(isTrain));  title('Avg');
figure; boxplot(fun.Xtrain, y(isTrain));  title('MAXFUN');


%% Classify
[avg.yHat, avg.metrics] = eval_svm(avg.Xtrain', y(isTrain), avg.Xtest', y(~isTrain));
[fun.yHat, fun.metrics] = eval_svm(fun.Xtrain', y(isTrain), fun.Xtest', y(~isTrain));

avg.metrics.acc
fun.metrics.acc
