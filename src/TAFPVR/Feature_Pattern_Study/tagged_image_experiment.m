% TAGGED_IMAGE_EXPERIMENT


% mjp, may 2016

%% Parameters
sigma = 2;
nTrials = 100;
nFolds = 5;

run_sift = @(I) sift_macrofeatures(I, 'subsamp', 1, 'step', 4, 'macrosl', 1);


%% Run Experiment
P = zeros(6,nTrials);

data.Xmax = zeros(nTrials*2,128);
data.Xfun = zeros(nTrials*2,128);
data.y = zeros(nTrials*2,1);

for ii = 1:nTrials
    fprintf('[%s]: starting trial %d (of %d)\n', mfilename, ii, nTrials);
    
    % Create two random images
    A = sigma * randn(200, 200);
    B = sigma * randn(200, 200);

    % Apply a small oscillatory "tag" within the interior of image A
    Tag = max(A(:))*real(make_plane_wave(10,10,30));
    x0 = ceil(rand()*120);
    y0 = ceil(rand()*120);
    A(x0:x0+29, y0:y0+29) = Tag;

    % Apply pooling to both images
    [mfA, nfoA] = spatial_pool(A, 'fun', 10);
    avgA = spatial_pool(A, 'avg');
    maxA = spatial_pool(A, 'max');

    [mfB, nfoB] = spatial_pool(B, 'fun', 10);
    avgB = spatial_pool(B, 'avg');
    maxB = spatial_pool(B, 'max');

    P(:,ii) = [mfA mfB avgA avgB maxA maxB]; 
   
    
    %% run SIFT
    Xa = run_sift(single(A));
    Xb = run_sift(single(B));
    
    % Apply pooling to both sets of features
    mfXa = spatial_pool(Xa, 'fun', 10);
    avgXa = spatial_pool(Xa, 'avg');
    maxXa = spatial_pool(Xa, 'max');

    mfXb = spatial_pool(Xb, 'fun', 10);
    avgXb = spatial_pool(Xb, 'avg');
    maxXb = spatial_pool(Xb, 'max');
    
    
    %% Store feature vectors
    idx = 2*(ii-1) + 1;
    data.y(idx) = 0;  
    data.y(idx+1) = 1;
    data.Xfun(idx,:) = mfXa;
    data.Xfun(idx+1,:) = mfXb;
    data.Xmax(idx,:) = maxXa;
    data.Xmax(idx+1,:) = maxXb;
    data.Xavg(idx,:) = avgXa;
    data.Xavg(idx+1,:) = avgXb;
end

data.foldId = assign_folds(data.y, nFolds);


%% Visualize (last iteration)

figure;
plot_mf_support(A, nfoA.row, nfoA.col, nfoA.w);
set(gca, 'Xtick', [], 'Ytick', []);
title('Example Image A');
saveas(gcf, 'imageA.eps', 'epsc');

figure(2);
plot_mf_support(B, nfoB.row, nfoB.col, nfoB.w);
set(gca, 'Xtick', [], 'Ytick', []);
title('Example Image B');
saveas(gcf, 'imageB.eps', 'epsc');
    
view_sift_features(Xa,2,2,A);
saveas(gcf, 'SIFT_classA.eps', 'epsc');

view_sift_features(Xb,2,2,B);
saveas(gcf, 'SIFT_classB.eps', 'epsc');

figure; boxplot(P', 'labels', {'MAXFUN A', 'MAXFUN B', 'avg A', 'avg B', 'max A', 'max B'});
title('raw image');
saveas(gcf, 'pooling_raw.eps', 'epsc');


% visualize class separation for different poolings.
D = pdist(data.Xfun, 'euclidean');
[Y,e] = cmdscale(D);
figure;
plot(Y(data.y==0,1), Y(data.y==0,2), 'bo', ...
     Y(data.y==1,1), Y(data.y==1,2), 'rx');
legend('class A', 'class B');
title('MDS for MAXFUN pooled features');
xlabel('x1');  ylabel('x2');
saveas(gcf, 'mds_maxfun.eps', 'epsc');

D = pdist(data.Xmax, 'euclidean');
[Y,e] = cmdscale(D);
figure;
plot(Y(data.y==0,1), Y(data.y==0,2), 'bo', ...
     Y(data.y==1,1), Y(data.y==1,2), 'rx');
legend('class A', 'class B');
xlabel('x1');  ylabel('x2');
title('MDS for max pooled features');
saveas(gcf, 'mds_max.eps', 'epsc');


D = pdist(data.Xavg, 'euclidean');
[Y,e] = cmdscale(D);
figure;
plot(Y(data.y==0,1), Y(data.y==0,2), 'bo', ...
     Y(data.y==1,1), Y(data.y==1,2), 'rx');
legend('class A', 'class B');
xlabel('x1');  ylabel('x2');
title('MDS for avg pooled features');
saveas(gcf, 'mds_avg.eps', 'epsc');



%% Run classification experiments
acc.fun = zeros(nFolds,1);
max.acc = zeros(nFolds,1);

for ii = 1:nFolds
    isTrain = (data.foldId == ii);
    yHat = eval_svm(data.Xfun(isTrain,:), data.y(isTrain), ...
                    data.Xfun(~isTrain,:), data.y(~isTrain));
    acc.fun(ii) = sum(yHat == data.y(~isTrain)) / sum(~isTrain);
    
    yHat = eval_svm(data.Xmax(isTrain,:), data.y(isTrain), ...
                    data.Xmax(~isTrain,:), data.y(~isTrain));
    acc.max(ii) = sum(yHat == data.y(~isTrain)) / sum(~isTrain);
end

