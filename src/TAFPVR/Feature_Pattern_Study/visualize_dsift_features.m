
% From the VLFeat documentation on SIFT descriptors:
% (http://www.vlfeat.org/api/sift.html#sift-intro-descriptor)
%
% """VLFeat SIFT descriptor uses the following convention. The y axis
% points downwards and angles are measured clockwise (to be consistent
% with the standard image convention). The 3-D histogram (consisting
% of 8×4×4=128 bins) is stacked as a single 128-dimensional vector,
% where the fastest varying dimension is the orientation and the
% slowest the y spatial coordinate. This is illustrated by the
% following figure."""

run_sift = @(I) sift_macrofeatures(I, 'subsamp', 1, 'step', 4, 'macrosl', 1);

%% Experimenting with dsift and feature "textures"

I = single(real(make_plane_wave(10,10)));
X = run_sift(I);
view_sift_features(X, 1, 1, I);
colormap(gray);


I = single(real(make_plane_wave(10,0)));
X = run_sift(I);
view_sift_features(X, 1, 1, I);
colormap(gray);


I = single(real(make_plane_wave(3,2)));
X = run_sift(I);
view_sift_features(X, 1, 1, I);
colormap(gray);

