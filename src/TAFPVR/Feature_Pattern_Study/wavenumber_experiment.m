% WAVENUMBER_EXPERIMENT   This is just to give some intuition into
% the wavenumber (spatial frequency) that is used for plane waves
% (e.g. in the Morlet filter).


n = 128;
a = inv(2*pi/n);  % make spatial domain span [-pi, pi].
                  % this should make wave numbers more interpretable

[X,Y] = make_image_domain([n,n], a, 'time');

K = [sqrt(3)  sqrt(3) ;
     0  3 ;
     3  0 ;
     sqrt(4) -sqrt(4)];

figure('Position', [200 300 950, 500]);  
ha = tight_subplot(2, size(K,1), [.05, .01]);

for ii = 1:size(K,1)
    k0 = K(ii,:);
    ks = sqrt(sum(k0.^2));
    theta = atan2d(k0(2), k0(1));
    Psi = exp(1i*(k0(1)*X + k0(2)*Y));  % exp(i <k0,[x y]>) 
    
    desc = sprintf('k0=(%0.1f,%0.1f), |k0|=%0.1f, theta=%0.1f', k0(1), k0(2), ks, theta);
    
    axes(ha(ii));
    imagesc(real(Psi), [-1 1]);
    set(gca, 'Xtick', [], 'Ytick', []);
    title(desc);
    if ii == 1, ylabel('real part'); end
    
    axes(ha(ii + size(K,1)));
    imagesc(imag(Psi), [-1 1]);
    set(gca, 'Xtick', [], 'Ytick', []);
    if ii == 1, ylabel('imag part'); end
end
colormap('bone');
suptitle('various plane waves');


