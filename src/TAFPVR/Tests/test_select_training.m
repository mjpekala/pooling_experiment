% mjp, april 2016

y = [ones(10,1) ; 2*ones(10,1) ; 3*ones(10,1)];

isTrain = select_training(y);
yTrain = y(isTrain);
yTest = y(~isTrain);

assert(sum(yTrain==1) == 5);
assert(sum(yTrain==2) == 5);
assert(sum(yTrain==3) == 5);
assert(sum(yTest==1) == 5);
assert(sum(yTest==2) == 5);
assert(sum(yTest==3) == 5);
