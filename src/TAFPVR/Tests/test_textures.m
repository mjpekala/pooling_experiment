
% From the VLFeat documentation on SIFT descriptors:
% (http://www.vlfeat.org/api/sift.html#sift-intro-descriptor)
%
% """VLFeat SIFT descriptor uses the following convention. The y axis
% points downwards and angles are measured clockwise (to be consistent
% with the standard image convention). The 3-D histogram (consisting
% of 8×4×4=128 bins) is stacked as a single 128-dimensional vector,
% where the fastest varying dimension is the orientation and the
% slowest the y spatial coordinate. This is illustrated by the
% following figure."""

% mjp, may 2016


% some helper functions
almost_equal = @(a,b) abs(a-b) < 1e-8;

sinusoid_image = @(f) repmat(sin(pi*f*(-10:.1:10)), 200, 1);


%% Experimenting with dsift and feature "textures"
run_sift = @(I) sift_macrofeatures(I, 'subsamp', 1, 'step', 4, 'macrosl', 1);
I = single(sinusoid_image(1));
X = run_sift(I);

figure; view_features(X);

figure; 
subplot(1,2,1); imagesc(I); title('image'); colorbar;
subplot(1,2,2); imagesc(X(:,:,1));  title('SIFT channel 1'); colorbar;



%% "Checkerboard" textures

% compare pooling on images that are either:
% a) salt and pepper noise or,
% b) a sparse checkerboard pattern.

n = 13;   % should be odd
sz = [200 300];
nTrials = 30;

P = zeros(nTrials, 3);

% build a checkerboard
C = reshape(mod(1:(n^2), 2), n, n);

for ii = 1:nTrials
    X = zeros(sz);
 
    if mod(ii,2) == 0
        idx = randsample(numel(X), sum(C(:)));
        X(idx) = 1;
    else
        x0 = ceil(rand() * (sz(2) - size(C,2) - 3));
        y0 = ceil(rand() * (sz(1) - size(C,1) - 3));
        X(y0:y0+size(C,1)-1, x0:x0+size(C,2)-1) = C;
        assert(all(size(X) == sz));
    end
 
    P(ii,1) = spatial_pool(X, 'avg');
    P(ii,2) = spatial_pool(X, 'max');
    P(ii,3) = spatial_pool(X, 'maxfun', floor(n/2));
end

% avg and max cannot distinguish these two patterns.
assert(all(almost_equal(P(:,1), P(1,1))));
assert(all(almost_equal(P(:,2), P(1,2))));

% this should look different to MAXFUN though
assert(~ almost_equal(P(1,3), P(2,3)));

figure;
plot(1:size(P,1), P(:,1), 'o-', ...
     1:size(P,1), P(:,2), 'o-', ...
     1:size(P,1), P(:,3), 'o-');
xlabel('');
ylabel('pool value');
legend('avg', 'max', 'maxfun');
title('pooling checkerboard or salt-and-pepper');



%% "grating" pattern
gaps = [0:10];

P = zeros(length(gaps), 6);
for ii = 1:length(gaps)
    X = zeros(100,100);
    [x0,y0] = deal(20,20);
  
    % construct "grating" pattern
    for jj = 0:9
        X(x0+jj*gaps(ii), y0:y0+30) = 1;
    end
    
    P(ii,1) = spatial_pool(X, 'avg');
    P(ii,2) = spatial_pool(X, 'max');
    P(ii,3) = spatial_pool(X, 'pnorm', 2);
    P(ii,4) = spatial_pool(X, 'maxfun', 10);
    P(ii,5) = spatial_pool(X, 'maxfun', 20);
    P(ii,6) = spatial_pool(X, 'maxfun', 30);
end

figure;
plot(gaps, P(:,1), 'o-', ...
     gaps, P(:,2), 'o-', ...
     gaps, P(:,3), 'o-', ...
     gaps, P(:,4), 'o-', ...
     gaps, P(:,5), 'o-', ...
     gaps, P(:,6), 'o-');
xlabel('grating gap');
ylabel('pool value');
ylim([0 1.1]);
legend('avg', 'max', 'sqrt', 'maxfun(10)', 'maxfun(20)', 'maxfun(30)', ...
       'Location', 'NorthEast');
title('pooling "grating" pattern');

hold on
axes('position',[.7, 0.4, 0.2, 0.25])
imagesc(X); set(gca, 'XTick', [], 'YTick', []);
hold off

saveas(gcf, 'grating.png', 'png');



%% periodic signals

aAll = .1:.05:1.2;
P = zeros(length(aAll), 8);
for ii = 1:length(aAll)
    X = sinusoid_image(aAll(ii));
    
    P(ii,1) = spatial_pool(X, 'avg');
    P(ii,2) = spatial_pool(X, 'max');
    P(ii,3) = spatial_pool(X, 'pnorm', 2);
    P(ii,4) = spatial_pool(X, 'maxfun', 10);
    P(ii,5) = spatial_pool(X, 'maxfun', 20);
    P(ii,6) = spatial_pool(X, 'maxfun', 30);
    P(ii,7) = spatial_pool(X, 'sos', 10);
    P(ii,8) = spatial_pool(X, 'pnorm', 4);
    
    if aAll(ii) == 1
        XtoPlot = X;
    end
end

figure;
plot(aAll, P(:,1), '--', ...
     aAll, P(:,2), '--', ...
     aAll, P(:,3), 'o-', ...
     aAll, P(:,4), 'o-', ...
     aAll, P(:,5), 'o-', ...
     aAll, P(:,6), 'o-', ...
     aAll, P(:,7), '.-', ...
     aAll, P(:,8), 'o-');
xlabel('a');
ylabel('pool value');
ylim([-.1 1.1]);
legend('avg', 'max', 'p-norm(2)', ...
       'maxfun(10)', 'maxfun(20)', 'maxfun(30)', 'sos(10)', ...
       'p-norm(4)', ...
       'Location', 'West');
title('pooling periodic pattern');

hold on
axes('position',[.7, 0.2, 0.2, 0.2])
imagesc(XtoPlot); set(gca, 'XTick', [], 'YTick', []);
hold off

saveas(gcf, 'sinusoid.png', 'png');
