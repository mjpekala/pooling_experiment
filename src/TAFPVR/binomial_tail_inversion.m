function p = binomial_tail_inversion(m, k, delta)
% BINOMIAL_TAIL_INVERSION  
%
%     BTI(m,k,delta) := max {p : Bin(m,k,p) > delta}      (1)
%
%     where k is the observed number of "successes" (in our context,
%     classification errors) out of m trials, p is the probability of
%     "success" (error) on a given Bernoulli trial and delta is a
%     confidence value.  
%
%     In other words, this function is estimating the largest true
%     error probability p such that the probability of observing k or
%     fewer errors in m trials is at least delta.
%
%     Hence, p provides a conservative upper bound on the "true" error
%     rate of a classifier.  Equation (1) is taken from definition 3.2
%     in [Langford].
%
%  References:
%  o John Langford "Tutorial on Practical Prediction Theory for
%    Classification," JMLR 2005. 

% mjp, april 2016

assert(0 <= k);
assert(k <= m);
assert(0 < delta && delta < 1);

lowerBound = k/m;
% see Def. 3.2 in Langford.
upperBound = k/m + sqrt(log(1/delta) / 2 / m);

pv = lowerBound:.001:upperBound;
y = cdf('Binomial', k, m, pv);
idx = find(y >= delta, 1, 'last');
p = pv(idx);

