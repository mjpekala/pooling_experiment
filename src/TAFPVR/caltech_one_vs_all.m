%  CALTECH_ONE_VS_ALL  
%
%   Compare the impact of different pooling strategies on one-vs-all
%   classification problems derived from Caltech 101.
%
%   Note that memory usage is a concern due to the size of the data
%   in this experiment.  Hence, we try to avoid loading and/or
%   retaining data unnecessarily.
%
%
% REFERENCES:
%  [1] Boureau et al. "A Theoretical Analysis of Feature Pooling in Visual Recognition," 2010.
%  [2] Boureau et al. "Learning Mid-Level Features For Recognition," 2010.
%  [3] Chatfield et al. "The devil is in the details..." BMVC 2011.

% mjp, april 2016

if ~exist('vl_dsift')
    error('could not find vl_dsift().  Did you remember to call setup?');
end



%% Experiment Parameters 

rng(9999);

if 1
    imageDir = '../../examples/caltech_101/101_ObjectCategories';
    sz = [200 300];               % assume fixed-size inputs in this script
    nTrain = 30;                  % see section 2.3 in [1] and also [3]
    nTest = 30;                   % see [3]
else
    imageDir = '../../examples/KTH_TIPS';
    sz = [200 200];
    nTrain = 10;
    nTest = 80 - nTrain;
end
    
nSplits = 5;                      % In section 2.3 in [1], the authors use 10 splits.
nAtoms = 128;


timestamp = datestr(now);
rootDir = ['Outputs_' timestamp];
if ~exist(rootDir), mkdir(rootDir); end
diary(fullfile(rootDir, ['log_all_vs_all_' timestamp '.txt']));

overallTimer = tic;

%% some helper functions

shuffle = @(x) x(randperm(numel(x)));
run_sift = @(I) sift_macrofeatures(I, 'subsamp', 1, 'step', 4, 'macrosl', 2);
featurize = @(fn) run_sift(read_images(fn, sz));

% take a tensor with dimensions:   (rows, cols, #features, #examples)
% and reshapes into a matrix:      (#features, #objects)
tensor_to_matrix = @(T) reshape(permute(T, [3 1 2 4]), size(T,3), size(T,1)*size(T,2)*size(T,4));

% un-does the action of tensor_to_matrix
matrix_to_tensor = @(M, sz) permute(reshape(M, size(M,1), sz(1), sz(2), sz(4)), [2 3 1 4]);


%% Load dataset 

% Get a list of all classes in the dataset.
% e.g. for Caltech 101, there will be 101 subdirectories.
classDirs = dir(imageDir);

% Omit the '.', '..' and any other special directories.
% Also exclude the background category (not technically one of the 101 classes).
% Also exclude any non-directory files.
included = logical(ones(length(classDirs),1));
for ii = 1:length(classDirs)
    if strcmp(classDirs(ii).name(1), '.')
        included(ii) = 0;
    elseif strcmp(classDirs(ii).name, 'BACKGROUND_Google')
        included(ii) = 0;
    elseif ~isdir(fullfile(imageDir, classDirs(ii).name))
        included(ii) = 0;
    end
end
classDirs = classDirs(included);



classNames = {};
for ii = 1:length(classDirs)
    classNames{ii} = classDirs(ii).name;
end


% get a list of all image filenames.
yAll = 1:length(classDirs);
y = [];
imageFiles = {};
for yi = yAll
    cclass = classDirs(yi).name;
    files = dir(fullfile(imageDir, cclass, '*.jpg'));
    if isempty(files), files = dir(fullfile(imageDir, cclass, '*.png')); end
    assert(~isempty(files));
    
    files = cellfun(@(ii) fullfile(imageDir, cclass, files(ii).name), ...
                    num2cell(1:length(files)), 'UniformOutput', 0);
    
    imageFiles = [imageFiles files];
    y = [y ; yi*ones(length(files),1)];
end

fprintf('[%s]: Data set has %d classes\n', mfilename, length(yAll));


%% Run Experiments

Sall = {};
for splitId = 1:nSplits
    fprintf('====================================================\n');
    fprintf('[%s]: Starting split %d (of %d)\n', mfilename, splitId, nSplits);
    fprintf('====================================================\n\n');
    
    experimentDir = fullfile(rootDir, sprintf('split_%0.2d_codewords_%d', splitId, nAtoms));
    if ~exist(experimentDir), mkdir(experimentDir); end

    %% Identify train/test split.  
    % The procedure of [1] is not fully % specified, so we also take
    % inspiration from [3].
    %
    isTrain = logical(zeros(size(y)));
    isTest = logical(zeros(size(y)));

    isTrain = select_training(nTrain);
    isTest = ~isTrain;
    %for ii = 1:length(yAll), yi=yAll(ii);
    %    idx = shuffle(find(y == yi));
    %    isTrain(idx(1:nTrain)) = 1;
    %    isTest(idx((nTrain+1):min(nTrain+nTest,length(idx)))) = 1;
    %end
    
    assert(sum(isTrain > 0) && sum(isTest > 0));

    
    %% generate low-level features for training data
    Xraw = cellfun_status(featurize, imageFiles(isTrain), 'SIFT train');
    Xraw = cat(4, Xraw{:});
    Xraw = double(Xraw);
    train.y = y(isTrain);

    
    %% learn dictionary
    lambdaAll = [.0001 .001 .01];
    lambdaAll = [.0001];   % XXX: hardcoded choice
    dlTimer = tic;
    [D, param] = learn_dictionary(tensor_to_matrix(Xraw), y, ...
                                  'k', 2, 'lambdas', lambdaAll, 'nAtoms', nAtoms);
    fprintf('[%s]: dictionary learning took %0.2f (min)\n', mfilename, toc(dlTimer)/60);
 
    
    %% Sparse coding (encode train and test data)
    fprintf('[%s]: encoding data using dictionary\n', mfilename);

    % Note: the following encodes each example independently. 
    %
    scTimer = tic;
    train.X = zeros(size(Xraw,1), size(Xraw,2), size(D,2), size(Xraw,4));
    for ii = 1:size(Xraw, 4)
        Xi = Xraw(:,:,:,ii);
        [height, width, chan] = size(Xi);
        Xi = reshape(Xi, height*width, chan);
        Ai = full(mexLasso(Xi', D, param));
        Ai = reshape(Ai', height, width, size(D,2));
        train.X(:,:,:,ii) = Ai;
    end
    clear Xi Ai Xraw;
    fprintf('[%s]: sparse coding (train data) took %0.2f (sec)\n', mfilename, toc(scTimer));
   
    % now the test data
    %
    Xraw = cellfun_status(featurize, imageFiles(isTest), 'SIFT test');
    Xraw = cat(4, Xraw{:});
    Xraw = double(Xraw);
    test.y = y(isTest);
    
    scTimer = tic;
    test.X = zeros(size(Xraw,1), size(Xraw,2), size(D,2), size(Xraw,4));
    for ii = 1:size(Xraw, 4)
        Xi = Xraw(:,:,:,ii);
        [height, width, chan] = size(Xi);
        Xi = reshape(Xi, height*width, chan);
        Ai = full(mexLasso(Xi', D, param));
        Ai = reshape(Ai', height, width, size(D,2));
        test.X(:,:,:,ii) = Ai;
    end
    clear Xi Ai Xraw;
    fprintf('[%s]: sparse coding (test data) took %0.2f (sec)\n', mfilename, toc(scTimer));


    save(fullfile(experimentDir, 'data.mat'), 'train', 'test', 'D', 'param', 'isTrain', 'isTest', '-v7.3');
    fprintf('[%s]: saved features in "%s"\n', mfilename, experimentDir);

 
    %% Pooling
    
    % Step 1: pooling parameter selection (for those operations that need it)
    %
    wi_sos_pool = @(X, k) spatial_pool(X, 'sos', k);
    wi_mf_pool = @(X, k) spatial_pool(X, 'fun', k);
       
    % assign folds so that all pooling functions see the same data splits. 
    pFoldId = assign_folds(train.y, 5);
        
    pcvTimer = tic;
    pMF = select_scalar_pooling_param(train.X, train.y, wi_mf_pool, 4:2:20, pFoldId); 
        
    % XXX: whether we should select pSOS separately or base it off the
    % MAXFUN value depends upon whether we are evaluating this as
    % a separate methodology or merely viewing it as an upper
    % bound on MAXFUN.
    pSOS = select_scalar_pooling_param(train.X, train.y, wi_sos_pool, 6:2:24, pFoldId); 
    %pSOS = pMF;

    fprintf('[%s]: runtime for pooling parameter selection: %0.2f (min)\n', ...
            mfilename, toc(pcvTimer)/60.);

    % Step 2: pool the features
    %
    % This is for "whole image" pooling.  The features from all spatial
    % regions are combined to form a single feature vector for each
    % object.
    train.avgpool = spatial_pool(train.X, 'avg');
    train.funpool  = spatial_pool(train.X, 'fun', pMF);
    train.maxpool = spatial_pool(train.X, 'max');
    train.sospool = spatial_pool(train.X, 'sos', pSOS);  
    train.sqrtpool = spatial_pool(train.X, 'pnorm', 2);

    test.avgpool = spatial_pool(test.X, 'avg');
    test.funpool  = spatial_pool(test.X, 'fun', pMF);
    test.maxpool = spatial_pool(test.X, 'max');
    test.sospool = spatial_pool(test.X, 'sos', pSOS);  
    test.sqrtpool = spatial_pool(test.X, 'pnorm', 2);

    
    %% Classification

    %
    % Since the sample sizes are so small, we'll store the raw
    % results (rather than accuracy values).  Otherwise, aggregate
    % statistics may be a bit misleading.
    %

    % the transposes below are because svmtrain wants rows-as-examples
    [yHat, avgp.metrics] = eval_svm(train.avgpool', train.y, test.avgpool', test.y);
    [yHat, fun.metrics] = eval_svm(train.funpool', train.y, test.funpool', test.y);
    [yHat, sos.metrics] = eval_svm(train.sospool', train.y, test.sospool', test.y);
    [yHat, maxp.metrics] = eval_svm(train.maxpool', train.y, test.maxpool', test.y);
    [yHat, sqrt.metrics] = eval_svm(train.sqrtpool', train.y, test.sqrtpool', test.y);
    
    % some aggregate statistics
    S = 100*[avgp.metrics.acc fun.metrics.acc sos.metrics.acc maxp.metrics.acc sqrt.metrics.acc];
    mr = mean_rank(S);

    %% Report results for this split
    fprintf('[%s]: avgpool acc / mean rank:   %0.2f%% / %0.2f\n', ...
            mfilename, 100*avgp.metrics.nCorrect/numel(test.y), mr(1));
    fprintf('[%s]: funpool acc / mean rank:   %0.2f%% / %0.2f\n', ...
            mfilename, 100*fun.metrics.nCorrect/numel(test.y), mr(2));
    fprintf('[%s]: sospool acc / mean rank:   %0.2f%% / %0.2f\n', ...
            mfilename, 100*sos.metrics.nCorrect/numel(test.y), mr(3));
    fprintf('[%s]: maxpool acc / mean rank:   %0.2f%% / %0.2f\n', ...
            mfilename, 100*maxp.metrics.nCorrect/numel(test.y), mr(4));
    fprintf('[%s]: sqrtpool acc / mean rank:  %0.2f%% / %0.2f\n', ...
            mfilename, 100*sqrt.metrics.nCorrect/numel(test.y), mr(5));
    
    fprintf('%19s (cnt) : %8s  %8s  %8s  %8s  %8s : funrank\n', ...
            'class', 'avg', 'fun', 'sos', 'max', 'sqrt');
    for ii = 1:size(S,1)
        tr = tiedrank(100 - S(ii,:));
        fprintf('%20s (%2d) : %8.2f  %8.2f  %8.2f  %8.2f  %8.2f : %0.1f\n', ...
                classNames{ii}, sum(test.y==ii), S(ii,1), S(ii,2), S(ii,3), S(ii,4), S(ii,5), tr(2));
    end
   
    Sall = [Sall S];
    
    save(fullfile(experimentDir, 'perf.mat'), 'S', 'mr', 'avgp', 'fun', 'sos', 'maxp', 'sqrt', '-v7.3');
end


fprintf('[%s]: total runtime %0.2f (min)\n', mfilename, toc(overallTimer)/60.);
save(fullfile(experimentDir, 'Sall.mat'), 'Sall', '-v7.3');

diary off;