%  CALTECH_ONE_VS_ONE Classification problem to distinguish between
%                     two classes from Caltech 101.
%
%  Our actual experiment involves calling this script for many
%  different class pairs.  
%
% REFERENCES:
%  [1] Boureau et al. "A Theoretical Analysis of Feature Pooling in Visual Recognition," 2010.
%  [2] Boureau et al. "Learning Mid-Level Features For Recognition," 2010.
%  [3] SPAMS toolbox (http://spams-devel.gforge.inria.fr/)

% mjp, april 2016

if ~exist('vl_dsift')
    error('could not find vl_dsift().  Did you remember to add vlfeat to matlabs search path (<VLFEATROOT>/toolbox/vl_setup)?');
end


%% Experiment Parameters and Setup

% Class names specified this way in order to (a) keep this file as a
% script (for interaction via Matlab shell) and (b) make it easy for
% other scripts to specify different experiments.
if ~isempty(getenv('CLASS_A'))
    classA = getenv('CLASS_A');
else
    %classA = 'cup';
    %classA = 'rhino';
    classA = 'crocodile';
end
if ~isempty(getenv('CLASS_B'))
    classB = getenv('CLASS_B');
else
    %classB = 'dalmatian';
    %classB = 'brontosaurus';
    classB = 'scorpion';
end


% These should remain fixed across all caltech 101 pairs.
sz = [200 300];  % Set this to [] if you do not want all images
                 % resized a-priori
caltechDir = '../../examples/caltech_101/101_ObjectCategories';
rootDir = 'Outputs';
nSplits = 10;           % In section 2.3 in [1], the authors use 10 splits.
nAtoms = 128;

overallTime = tic;

experimentDir = fullfile(rootDir, sprintf('%s_vs_%s_%d_codewords', classA, classB, nAtoms));
if ~exist(experimentDir), mkdir(experimentDir); end


%% Load images for 1-vs-1 classification problem
fprintf('[%s]: loading raw images for "%s" vs "%s"...\n', mfilename, classA, classB);

Ia = read_caltech101(fullfile(caltechDir, classA), sz);    
ya = zeros(length(Ia),1);

Ib = read_caltech101(fullfile(caltechDir, classB), sz);    
yb = ones(length(Ib),1);

I = [Ia Ib];  y = [ya ; yb];

clear Ia Ib ya yb;

% shuffle examples
idx = randperm(length(y));
I = I(idx);
y = y(idx);



%% Featurize
fprintf('[%s]: generating SIFT features for %d images\n', mfilename, length(y));
low_level_feat_func = @(I) sift_macrofeatures(I, 'subsamp', 1, 'step', 4, 'macrosl', 2);

featTimer = tic;
Xraw = cellfun(low_level_feat_func, I, 'UniformOutput', 0);
fprintf('[%s]: low-level feature generation took %0.2f (sec)\n', mfilename, toc(featTimer));

fprintf('[%s]: maximum possible pooling cardinality is: P=%d\n', ...
        mfilename, size(Xraw{1},1)*size(Xraw{1},2));

% For now, I insist all images be the same size
Xraw = cat(4, Xraw{:});

% convert all data to double
% Do this *after* running SIFT (which requires single data)
Xraw = double(Xraw);


%% Dictionary Learning

% Conceptually, this step is taking the SIFT feature dimension and
% replacing it with a new "codebook" dimension.
%
% I argue that since the dictionary learning and sparse coding
% procedures are not using information about class labels it is
% essentially an unsupervised technique and therefore it is reasonable
% to use all the data (i.e. we need not worry about train/test splits
% at this point).
%


% take a tensor with dimensions:   (rows, cols, SIFT_features, #examples)
% and reshapes into a matrix:      (SIFT_features, #objects)
tensor_to_matrix = @(M) reshape(permute(M, [3 1 2 4]), size(M,3), size(M,1)*size(M,2)*size(M,4));
matrix_to_tensor = @(T, sz) permute(reshape(T, size(T,1), sz(1), sz(2), sz(4)), [2 3 1 4]);

dlTimer = tic;

if 0   
    % hyper-parameter selection
    [D, param] = learn_dictionary(tensor_to_matrix(Xraw), ...
                                  'k', 5, 'lambdas', [.0001 .001 .01], 'nAtoms', nAtoms);
else
    % hardcoded choice for ell-1 regularization parameter
    [D, param] = learn_dictionary(tensor_to_matrix(Xraw), y, ...
                                  'k', 2, 'lambdas', [.0001], 'nAtoms', nAtoms);
end
    
fprintf('[%s]: dict learning took %0.2f (sec)\n', mfilename, toc(dlTimer));

    
%% Sparse coding (encode train and test data)
fprintf('[%s]: encoding data using dictionary\n', mfilename);
    
% Note: the following encodes each example independently. 
%
Alpha = zeros(size(Xraw,1), size(Xraw,2), size(D,2), size(Xraw,4));

scTimer = tic;
for ii = 1:size(Xraw,4)
    Xi = Xraw(:,:,:,ii);
    [height, width, chan] = size(Xi);
    Xi = reshape(Xi, height*width, chan);
    Ai = full(mexLasso(Xi', D, param));
    Ai = reshape(Ai', height, width, size(D,2));
    Alpha(:,:,:,ii) = Ai;
end

fprintf('[%s]: sparse coding took %0.2f (sec)\n', mfilename, toc(scTimer));

save(fullfile(experimentDir, 'data.mat'), 'Xraw', 'D', 'param', 'Alpha');
fprintf('[%s]: saved features in "%s"\n', mfilename, experimentDir);


if any(Alpha(:) < 0)
    fprintf('==========================================================\n');
    fprintf('[%s]: it appears you did not use a non-negativity sparse coding constraint\n', mfilename);
    fprintf('[%s]: Are you sure this is what you want?\n', mfilename);
    fprintf('==========================================================\n');
end



%% Run the experiment for a number of train/test splits

YhatAll = [];
Acc = zeros(5, nSplits);
PoolParam = zeros(2, nSplits);

for splitId = 1:nSplits
    fprintf('\n[%s]: --= "%s" vs "%s" : split %d (of %d) =--\n\n', ...
            mfilename, classA, classB, splitId, nSplits);
    
    outDir = fullfile(experimentDir, sprintf('Split%02d', splitId));
    if ~exist(outDir), mkdir(outDir); end

    
    %% Split data into train/test
    
    % NOTE: could sweep over nTrain to see if/how any difference in
    % pooling strategies varies as a function of data set size.
    %
    nTrain = 30;  % (see section 2.3 in [1])
    train.bits = select_training(y, ceil(nTrain/2));
    train.X = Alpha(:,:,:,train.bits);
    train.y = y(train.bits);
    test.X = Alpha(:,:,:,~train.bits);
    test.y = y(~train.bits);

    fprintf('\n[%s]: %d train examples and %d test examples (dim=%d)\n', ...
            mfilename, sum(train.bits), sum(~train.bits), size(Alpha,3));

    %% Pooling
    
    % TODO: analysis over multiple pooling cardinalities!!
    % For now, we only consider whole image pooling.

    % Step 1: pooling parameter selection (for those operations that need it)
    %
    if splitId >= 1
        wi_sos_pool = @(X, k) spatial_pool(X, 'sos', k);
        wi_mf_pool = @(X, k) spatial_pool(X, 'fun', k);
       
        % assign folds so that all pooling functions see the same data splits. 
        pFoldId = assign_folds(train.y, 5);
        
        pcvTimer = tic;
        pMF = select_scalar_pooling_param(train.X, train.y, wi_mf_pool, 4:2:20, pFoldId); 
        
        % XXX: whether we should select pSOS separately or base it off the
        % MAXFUN value depends upon whether we are evaluating this as
        % a separate methodology or merely viewing it as an upper
        % bound on MAXFUN.
        pSOS = select_scalar_pooling_param(train.X, train.y, wi_sos_pool, 6:2:24, pFoldId); 
        %pSOS = pMF;

        fprintf('[%s]: runtime for pooling parameter selection: %0.2f (min)\n', ...
                mfilename, toc(pcvTimer)/60.);

        PoolParam(1,splitId) = pMF;
        PoolParam(2,splitId) = pSOS;
    end

    % Step 2: pool the features
    %
    % This is for "whole image" pooling.  The features from all spatial
    % regions are combined to form a single feature vector for each
    % object.
    train.avgpool = spatial_pool(train.X, 'avg');
    train.funpool  = spatial_pool(train.X, 'fun', pMF);
    train.maxpool = spatial_pool(train.X, 'max');
    train.sospool = spatial_pool(train.X, 'sos', pSOS);  
    train.sqrtpool = spatial_pool(train.X, 'pnorm', 2);

    test.avgpool = spatial_pool(test.X, 'avg');
    test.funpool  = spatial_pool(test.X, 'fun', pMF);
    test.maxpool = spatial_pool(test.X, 'max');
    test.sospool = spatial_pool(test.X, 'sos', pSOS);  
    test.sqrtpool = spatial_pool(test.X, 'pnorm', 2);


    %% Classification

    % Use a linear SVM.  Could consider other classifiers; [2] suggested
    % that the intersection SVM did not seem to perform substantially
    % differently from linear SVM.
    %
    % Since the sample sizes are so small, we'll store the raw
    % results (rather than accuracy values).  Otherwise, aggregate
    % statistics may be a bit misleading.
    %
    Yhat = zeros(numel(test.y), 6);

    % the transposes below are because svmtrain wants rows-as-examples
    [Yhat(:,1), metrics] = eval_svm(train.avgpool', train.y, test.avgpool', test.y);
    fprintf('[%s]: (acc,p) for avgpool:      (%0.2f%%, %0.3f)\n', mfilename, metrics.acc, metrics.p);
    Acc(1,splitId) = metrics.acc;

    [Yhat(:,2), metrics] = eval_svm(train.funpool', train.y, test.funpool', test.y);
    fprintf('[%s]: (acc,p) for MAXFUN:       (%0.2f%%, %0.3f)\n', mfilename, metrics.acc, metrics.p);
    Acc(2,splitId) = metrics.acc;
 
    [Yhat(:,3), metrics] = eval_svm(train.sospool', train.y, test.sospool', test.y);
    fprintf('[%s]: (acc,p) for sos:          (%0.2f%%, %0.3f)\n', mfilename, metrics.acc, metrics.p);
    Acc(3,splitId) = metrics.acc;
 
    [Yhat(:,4), metrics] = eval_svm(train.maxpool', train.y, test.maxpool', test.y);
    fprintf('[%s]: (acc,p) for maxpool:      (%0.2f%%, %0.3f)\n', mfilename, metrics.acc, metrics.p);
    Acc(4,splitId) = metrics.acc;
 
    [Yhat(:,5), metrics] = eval_svm(train.sqrtpool', train.y, test.sqrtpool', test.y);
    fprintf('[%s]: (acc,p) for sqrtpool:     (%0.2f%%, %0.3f)\n', mfilename, metrics.acc, metrics.p);
    Acc(5,splitId) = metrics.acc;
   
    % mu = lambda^{-1}
    LambdaTrain = 1./ train.avgpool;  LambdaTrain = min(LambdaTrain, 1000);
    LambdaTest = 1 ./ test.avgpool;   LambdaTest = min(LambdaTest, 1000);
    [Yhat(:,6), metrics] = eval_svm(LambdaTrain', train.y, LambdaTest', test.y);
    fprintf('[%s]: (acc,p) for MLE_EXP:      (%0.2f%%, %0.3f)\n', mfilename, metrics.acc, metrics.p);
    Acc(6,splitId) = metrics.acc;
 
    save(fullfile(outDir, 'estimates.mat'), 'Yhat', 'test', 'pSOS', 'pMF');

    YhatAll = [YhatAll ; Yhat];
end

parentDir = fileparts(outDir);
save(fullfile(parentDir, 'estimates.mat'), 'YhatAll', 'PoolParam');

fprintf('[%s]: overall performance (across all splits):\n', mfilename);
fprintf('      avg:    %0.2f%% (%0.3f)\n', mean(Acc(1,:)), 2*std(Acc(1,:)));
fprintf('      maxfun: %0.2f%% (%0.3f)\n', mean(Acc(2,:)), 2*std(Acc(2,:)));
fprintf('      sos:    %0.2f%% (%0.3f)\n', mean(Acc(3,:)), 2*std(Acc(3,:)));
fprintf('      max:    %0.2f%% (%0.3f)\n', mean(Acc(4,:)), 2*std(Acc(4,:)));
fprintf('      sqrt:   %0.2f%% (%0.3f)\n', mean(Acc(5,:)), 2*std(Acc(5,:)));
fprintf('      MLE:    %0.2f%% (%0.3f)\n', mean(Acc(6,:)), 2*std(Acc(6,:)));

fprintf('[%s]: net runtime for this experiment: %0.2f (min)\n', mfilename, toc(overallTime)/60.);

if 0
    figure; 
    scatter(PoolParam(1,:), Acc(2,:));
    xlabel('min pool cardinality (sqrt)');
    ylabel('accuracy');
    title(sprintf('MAXFUN: hyperparameter vs performance (%s vs %s)', ...
                  classA, classB));
    
    figure; 
    scatter(PoolParam(2,:), Acc(3,:));
    xlabel('tail size');
    ylabel('accuracy');
    title(sprintf('SOS: hyperparameter vs performance (%s vs %s)', ...
                  classA, classB));
    
    h = view_features(Alpha(:,:,:,1));
end

