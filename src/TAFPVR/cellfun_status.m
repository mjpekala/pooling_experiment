function out = cellfun_status(func, celldata, name, varargin)
% CELLFUN_STATUS  Like cellfun, but with some status reporting

out = cell(length(celldata),1);

start = tic;
lastChatter = 0;

for ii = 1:length(celldata)
    if (ii == 1) || (toc(start) - lastChatter > 30),
        fprintf('  (%s) : %d (of %d) in %0.2f min\n', name, ii, length(celldata), toc(start)/60);
        lastChatter = toc(start);
    end
    
    out{ii} = func(celldata{ii});
end

fprintf('  (%s) : total time %0.2f (min)\n', name, toc(start)/60.);


