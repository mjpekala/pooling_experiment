function out = mean_rank(X)

% Note: this code deals with ties properly.  This is important so
% as not to penalize algorithms based on the order they appear as
% columns in X.
Y = zeros(size(X));
for ii = 1:size(X,1)
    Y(ii,:) = tiedrank(100 - X(ii,:));
end

out = mean(Y,1);

