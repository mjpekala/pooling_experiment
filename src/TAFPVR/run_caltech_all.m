% Runs 1-vs-1 classification problems that can be derived from the
% Caltech 101 dataset.

% mjp, april 2016



%% Get a list of all classes in the Caltech 101 dataset
caltechDir = '../../examples/caltech_101/101_ObjectCategories';

files = dir(caltechDir);

% omit the '.', '..' and any other special directories
included = logical(ones(length(files),1));
for ii = 1:length(files)
    if strcmp(files(ii).name(1), '.')
        included(ii) = 0;
    end
end
files = files(included);


%% Choose a subset of all (100 2) possible classification problems.
% If it seems worthwhile, can expand this to exhaustive
% enumeration.

rng(9999);                      % for reproducible results
nExperiments = 50;
C = combnk(1:length(files)-1, 2);
idx = randperm(size(C,1));   
C = C(idx(1:nExperiments),:);

timestamp = datestr(now);

diaryFile = ['log_all_' timestamp '.txt'];
diary(diaryFile);

AccAll = [];

for ii = 1:nExperiments
    fprintf('----------------------------------------------------\n');
    fprintf('[%s]: starting experiment %d (of %d)\n', mfilename, ii, nExperiments);
    fprintf('----------------------------------------------------\n');

    classA = files(C(ii,1)).name;
    classB = files(C(ii,2)).name;
        
    % pass "arguments" to caltech_one_vs_one script.
    setenv('CLASS_A', classA);
    setenv('CLASS_B', classB);

    run caltech_one_vs_one;
        
    setenv('CLASS_A', '');
    setenv('CLASS_B', '');
    
    AccAll = [AccAll Acc];
    
    % save results incrementally, in case of issues.
    save(['acc_' timestamp '.mat'], 'AccAll');
end

fprintf('[%s]: overall performance (across all experiments):\n', mfilename);
fprintf('      avg:    %0.2f%% (%0.3f)\n', mean(AccAll(1,:)), 2*std(AccAll(1,:)));
fprintf('      maxfun: %0.2f%% (%0.3f)\n', mean(AccAll(2,:)), 2*std(AccAll(2,:)));
fprintf('      sos:    %0.2f%% (%0.3f)\n', mean(AccAll(3,:)), 2*std(AccAll(3,:)));
fprintf('      max:    %0.2f%% (%0.3f)\n', mean(AccAll(4,:)), 2*std(AccAll(4,:)));
fprintf('      sqrt:   %0.2f%% (%0.3f)\n', mean(AccAll(5,:)), 2*std(AccAll(5,:)));
fprintf('      MLE:    %0.2f%% (%0.3f)\n', mean(AccAll(6,:)), 2*std(AccAll(6,:)));

diary off;

figure; 
boxplot(AccAll', 'labels', {'avg', 'fun', 'sos', 'max', 'sqrt', 'mle'});
saveas(['results_' timestamp '.fig'], 'fig');
saveas(gca, ['results_' timestamp '.png'], 'png');
