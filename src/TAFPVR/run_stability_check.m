% RUN_STABILITY_CHECK

rng(9999);

diaryFile = ['log_stability_check_' datestr(now) '.txt'];
diary(diaryFile);

setenv('CLASS_A', '');
setenv('CLASS_B', '');
for ii = 1:10
    run caltech_one_vs_one;
end

diary off;
