function [h, h2, h3] = view_features(X)
% VIEW_FEATURES  Visualize feature data (pre or post pooling) for a
%                single object.
%
%    X := a tensor of features with dimensions (height, width, #_channels)
%
%   Requires tight_subplot.

% mjp, april 2016


[xMin, xMax] = deal(min(X(:)), max(X(:)));
dim = ceil(sqrt(size(X,3)));

showit = @(X) set(get(imagesc(X, [xMin, xMax]), 'Parent'), 'XTick', [], 'YTick', []);


% Visualize spatial distribution of features
h = figure('Position', [100 100 1200 1200]);
ha = tight_subplot(dim, dim, [.01 .01], [.01 .05], .01);
for ii = 1:size(X,3)
    axes(ha(ii));  showit(X(:,:,ii)); 
    text(1, 5, num2str(ii));
end


% Visualize magnitudes of relative feature dimensions
xFeat = squeeze(sum(sum(X,1),2));
h2 = figure; 
stem(xFeat);  xlabel('feature idx'); ylabel('net "energy"');

% Visualize distributions within each dimension.
h3 = figure;
Xbp = reshape(X, size(X,1)*size(X,2), size(X,3));
boxplot(Xbp);
