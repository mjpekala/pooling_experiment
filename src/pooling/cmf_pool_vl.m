function [Xout, Rout] = cmf_pool_vl(X, rMax, dzdy, rMin)
% CMF_POOL_VL  A version of centered maximal function (CMF) pooling 
%              that uses vl_nnpool under the hood.
%
% PARAMETERS:
%      X    : a (H x W x D x N) tensor of N signals each of which has
%             height H, width W and D channels.
%
%      rMax : The maximum function filter radius (an integer >= 1).
%             Note that this choice for r also implies a particular
%             stride/downsampling rate.  Each square Q_{m,n} with
%             side length 2*rMax+1 will be mapped to a single
%             output pixel.
%
%      dzdy : (optional) the gradient from the tail of the CNN for a backward
%             pass computation; if empty, this function performs a
%             forward pass.
%
%      rMin : (optional) The minimum radius to consider for the 
%             discrete maximal function (integer >= 0).  Defaults
%             to 0.  Must be <= rMax.
%             
%
% NOTES:
%  o  Unlike average pooling, here the gradient depends upon which radius
%     was selected at each point.  I'm not sure if there's an easier way
%     to determine this without doing an forward pass to figure it
%     out...so for the time being it looks as though we are stuck doing a
%     forward pass no matter what.  

% mjp, April 2016
%

% Note: we do not use inputParser because it is too slow.
if nargin < 4, rMin = 0; end;
if nargin < 3, dzdy = []; end

assert(numel(rMax) == 1);
assert(0 <= rMin);
assert(rMin <= rMax);
stride = 2*rMax+1;
rVals = rMin:rMax;

% For now we assume the input signal is non-negative.
% This is not a strict requirement; to relax it, we just need to
% tweak the gradient to include the sign of the signal.
assert(all(X(:) >= 0));  


% Determine how many filters fit within the domain of the input image.
m = floor(size(X,1) / stride);
n = floor(size(X,2) / stride);

%----------------------------------------
% forward and backward pass.
%----------------------------------------
Xfilt = zeros(m, n, size(X,3), size(X,4), numel(rVals));
Dfilt = zeros(size(X,1), size(X,2), size(X,3), size(X,4), numel(rVals));

for ii = 1:numel(rVals), r = rVals(ii);
    % need to crop and possibly pad Xi so that vl_nnpool starts at the
    % correct place and computes the correct number of sliding window
    % values.
    dr = rMax - r;
    hEnd = m * stride - dr;
    wEnd = n * stride - dr;
    
    if hEnd <= size(X,1)
        padH = 0;
    else
        hEnd = size(X,1);
        padH = hEnd - size(X,1);
    end
    
    if wEnd <= size(X,2)
        padW = 0;
    else
        wEnd = size(X,2);
        padW = wEnd - size(X,2);
    end
   
    % do it
    Xi = X(1+dr:hEnd, 1+dr:wEnd, :, :);
    Xfilt(:,:,:,:,ii) = vl_nnpool(Xi, 2*r+1, ...
                                  'stride', stride, ...
                                  'pad', [0 padH 0 padW], ...
                                  'method', 'avg');
    
    if ~isempty(dzdy)
        Di = vl_nnpool(Xi, 2*r+1, dzdy, 'stride', stride, 'pad', [0 padH 0 padW], 'method', 'avg');
        % adjust for rows/cols removed from X when creating Xi
        Di = pad_tensor(Di, [dr  size(X,1)-hEnd   dr  size(X,2)-wEnd]);
        Dfilt(:,:,:,:,ii) = Di;
    end
end

% apply definition of discrete maximal operator
[V,Ridx] = max(Xfilt, [], 5);
Rout = rVals(Ridx);  % map indices to radius values


if isempty(dzdy)
    %----------------------------------------
    % Forward pass
    %----------------------------------------
    % (already done) 
    Xout = V;
else
    %----------------------------------------
    % Backward pass
    %----------------------------------------
    if numel(rVals) == 1
        % special case: if there was only one value of r there is no indexing
        % that needs to be done.  This generally will not be the case
        % (otherwise cmf is just average pooling) but does happen when
        % we run unit tests.
        Xout = Dfilt;
    else
        % need to select appropriate subset of Dfilt
        if stride > 1
            % adjust for stride by expanding rows and columns of Ridx.
            % TODO: replace repmat
            er = repmat(1:size(Ridx,1), stride, 1);  er = er(:);
            ec = repmat(1:size(Ridx,2), stride, 1);  ec = ec(:)';
        
            Ridx = Ridx(er, :, :, :, :);
            Ridx = Ridx(:, ec, :, :, :);
        end
    
        % adjust for any pixels lost due to rMax not dividing size(X) cleanly.
        chopW = mod(size(X,2), stride);
        chopH = mod(size(X,1), stride);
        Ridx = pad_tensor(Ridx, [0 chopH 0 chopW]);

        Xout = tensor_slice(Dfilt, Ridx);
    end
end
