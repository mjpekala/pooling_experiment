function X = pad_tensor(X, sz, scale)
% PAD_TENSOR  Duplicates border rows and cols of image tensor X
%             by a specified amount.
% 
if nargin < 3, scale = 1; end

if numel(sz) == 1
    sz = sz * [1 1 1 1];
end

% Special case - not asked to do any padding.
if all(sz == 0)
    return;
end



top = sz(1);
bottom = sz(2);
left = sz(3);
right = sz(4);

if top > 0
    v = X(1,:,:,:) * scale;
    V = v(ones(top,1), :, :, :);
    X = cat(1, V, X);
end

if bottom > 0
    v = X(end,:,:,:) * scale;
    V = v(ones(bottom,1), :, :, :);
    X = cat(1, X, V);
end

if left > 0
    v = X(:,1,:,:) * scale;
    V = v(:,ones(left,1), :, :);
    X = cat(2, V, X);
end

if right > 0
    v = X(:,end,:,:) * scale;
    V = v(:,ones(right,1), :, :);
    X = cat(2, X, V);
end

