% TEST_CMF_CIFAR   Test alternative pooling on many CIFAR-10 examples.
%
%  Assumes you have downloaded the matlab version of CIFAR-10.
%  An example script lives in examples/cifar_10.

% mjp, april 2015

cifarDir = fullfile('..', '..', 'examples', 'cifar_10', 'cifar-10-batches-mat');
cifarFile = fullfile(cifarDir, 'test_batch.mat');

rMax = 1;

if exist(cifarFile)
    test = load(cifarFile);

    X = reshape(test.data, size(test.data,1), 32, 32, 3);
    X = permute(X, [3 2 4 1]);
    X = double(X);  % from uint8
    Xmax = vl_nnpool(X, 2*rMax+1, 'stride', 2*rMax+1, 'method', 'max');
    Xavg = vl_nnpool(X, 2*rMax+1, 'stride', 2*rMax+1, 'method', 'avg');
    [Xcmf, Rcmf] = cmf_pool_vl(X, rMax);
    
    assert(all(Xmax(:) >= Xcmf(:)));
    assert(all(Xcmf(:) >= Xavg(:)));
end


for ii = [1 10 1000]
    figure('Position', [100 100 1500 300]);
    ha = tight_subplot(1, 5, [.01 .01], [.01 .05], .01);

    axes(ha(1));
    imagesc(X(:,:,1,ii), [0 255]);  colorbar();
    title('input image');

    axes(ha(2));
    imagesc(Xavg(:,:,1,ii), [0 255]);  colorbar();
    title(sprintf('average pooling, r=%d', rMax));

    axes(ha(3));
    imagesc(Xmax(:,:,1,ii), [0 255]);  colorbar();
    title(sprintf('max pooling, r=%d', rMax));

    axes(ha(4));
    imagesc(Xcmf(:,:,1,ii), [0 255]); colorbar();
    title(sprintf('CMF pooling, rMax=%d', rMax));
    
    axes(ha(5));
    imagesc(Rcmf(:,:,1,ii), [0 rMax]); colorbar();
    title(sprintf('CMF pooling, r value'));
    
    colormap('bone');
end