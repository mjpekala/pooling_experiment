% Unit tests for the cmf_pool_vl() function.
%


%% Test with a fixed matrix

% expect r=0
A = [ 1 1 1 ;
      1 1 1 ;
      1 1 1];

% The 1,1 element should induce r=1
B = [1e3  1    1
     1     10   1
     1     1    1];

% (26 / 9) > 2  => r=1
C = [3  3  3
     3  2  3
     3  3  3];

% the 3,3 element should induce r=1
D = [1  1  1
     1  3  1
     1  1  1e4];

[Xcmf,Rcmf] = cmf_pool_vl([ A B ; C D], 1);
assert(all(Rcmf(:) == [0 1 1 1]'));

% Test the minimum radius parameter
[Xcmf,Rcmf] = cmf_pool_vl([ A B ; C D], 1, [], 1);
assert(all(Rcmf(:) == [1 1 1 1]'));


%% Compare to max and average pooling

if ~exist('vl_nnpool')
    error('cannot find vl_nnpool; did you forget to run the setup() script?');
end


rMax = 5;

for shaveR = [0 1 2 5 10]
    shave = [shaveR 0 0 0];  % use this to check different image sizes

    %--------------------------------------------------
    % Visualize action of maximum pooling filter (forward pass)
    %--------------------------------------------------
    X = double(imread('cameraman.tif'));
    X = unpad_tensor(X, shave);
    X = cat(3, X, X, X);  % simulate multi-channel data
    X = cat(4, X, X);     % simulate multi-example data

    Xavg = vl_nnpool(X, 2*rMax+1, 'stride', 2*rMax+1, 'method', 'avg');
    Xmax = vl_nnpool(X, 2*rMax+1, 'stride', 2*rMax+1, 'method', 'max');
    [Xcmf,Rcmf] = cmf_pool_vl(X, rMax);
 
    [XcmfAvg,RcmfAvg] = cmf_pool_vl(X, rMax, [], rMax);  % simulates avg pooling

    % pick one channel (they're all the same)
    Xavg = Xavg(:,:,1,1);  Xmax = Xmax(:,:,1,1);  
    Xcmf = Xcmf(:,:,1,1);  XcmfAvg = XcmfAvg(:,:,1,1);

    % It should always be the case that:
    %   cmf pooling <= maximum pooling 
    %   avg pooling =< cmf pooling
    %
    % Verify this property holds here.
    assert(all(Xcmf(:) <= Xmax(:)));
    assert(all(Xavg(:) <= Xcmf(:)));
    
    % When we force rMin=rMax, this is just average pooling
    assert(all(abs(XcmfAvg(:) - Xavg(:)) < 1e-6));
end


% show plots (for one run only)
if exist('tight_subplot')
    figure('Position', [100 100 1200 300]);
    ha = tight_subplot(1, 4, [.05 .05], .08, .08);

    axes(ha(1));
    imagesc(X(:,:,1,1), [0 255]);  colorbar();
    title('input image');

    axes(ha(2));
    imagesc(Xavg, [0 255]);  colorbar();
    title(sprintf('average pooling, r=%d', rMax));

    axes(ha(3));
    imagesc(Xmax, [0 255]);  colorbar();
    title(sprintf('max pooling, r=%d', rMax));

    axes(ha(4));
    imagesc(Xcmf, [0 255]); colorbar();
    title(sprintf('CMF pooling, rMax=%d', rMax));
end



%-------------------------------------------------------------------------------
% Make sure backward pass produces result with appropriate dimensions.
%-------------------------------------------------------------------------------
dzdy = randn(size(cmf_pool_vl(X, rMax))); % arbitrary data - just
                                          % needs to be the right size
dzdx = cmf_pool_vl(X, rMax, dzdy);

% make sure having a single r-value still works
dzdx = cmf_pool_vl(X, rMax, dzdy, rMax);


fprintf('[%s]: all tests passed!\n', mfilename);