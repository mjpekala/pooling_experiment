% Unit tests for the pad_tensor() function.
%

Img = imread('cameraman.tif');

%--------------------------------------------------
% 2d image case
%--------------------------------------------------
Tmp = pad_tensor(Img, 0);
assert(all(Img(:) == Tmp(:)));

n = 10;
Tmp = pad_tensor(Img, n);
assert(all(all(Img == Tmp(n+1:end-n, n+1:end-n))));

%--------------------------------------------------
% A full 4d tensor
%--------------------------------------------------
as_col = @(x) x(:);   % stupid matlab 
Img = randn(100,100,3,10);

Tmp = pad_tensor(Img, 0);
assert(all(Img(:) == Tmp(:)));

n = 10;
Tmp = pad_tensor(Img, [n 0 0 0], 0);
assert(all(as_col(Tmp(n,:,:,:) == 0)));
Tmp = Tmp(n+1:end,:,:,:);
assert(all(Img(:) == Tmp(:)));
