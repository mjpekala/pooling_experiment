function Xout = unpad_tensor(X, pad)
% UNPAD_TENSOR  Removes padding from an image tensor.
%
%      X : a (H x W x ...) tensor of images each of which has
%          height H and width W.
%
%      pad : padding size array of the form [TOP BOTTOM LEFT RIGHT]
%            or a scalar pad that should be removed from all sides.

%
% mjp, march 2016

% Special case - no padding
if all(pad == 0)
    Xout = X;
    return;
end

if numel(pad) == 1
    pad = [pad pad pad pad];
end

%--------------------------------------------------
% do it
%--------------------------------------------------
top = pad(1);
bottom = pad(2);
left = pad(3);
right = pad(4);

% This tailShape variable is to accomodate X with an arbitrary
% number of dimensions (beyond 2)
tailShape = size(X);
tailShape = tailShape(3:end);

Xout = reshape(X, size(X,1), size(X,2), prod(tailShape));
Xout = Xout(1+top:end-bottom, 1+left:end-right,:);
Xout = reshape(Xout, [size(Xout,1), size(Xout,2), tailShape]);
