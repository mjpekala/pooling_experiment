# Pooling Implementations

## Average Pooling

This directory contains an example of a pure Matlab implementation of an average pooling operator.  It provides a strict subset of the capability of MatConvNet's vl_nnpool(..., 'Method', 'avg') function.  One would never use this in practice (vl_nnpool is more efficient); however, it does provide a basic example of how one might implement the forward and backward pass of a simple pooling operator.  

See the unit test "test_avg_pool.m" for some examples of how the API compares to vl_nnpool in a stand-alone environment.  You can also look at ../examples directory to see examples of how this function can be used within a convnet.

Running the aforementioned unit test should produce stdout that looks something like:

```
    >> test_avg_pool
    [test_avg_pool] X has size: 100  100    3
    [test_avg_pool] Will run 10000 iterations of each implementation (for timing purposes)
    [test_avg_pool] Testing forward pass...
    [test_avg_pool]: runtime for matconvnet:             2.89 sec
    [test_avg_pool]: runtime for sparse matrix multiply: 3.42 sec
```

With some care (e.g. caching S) this pure matlab implementation is not that much slower than the native c++ matconvnet implementation.
