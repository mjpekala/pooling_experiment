function [Y, St] = avg_pool(X, poolSize, dzdy, opts)
% AVG_POOL  A pure matlab implementation of average pooling.
%
%   This function replicates a subset of the functionality provided by
%   vl_nnpool().  For actual applications, one would use vl_nnpool();
%   this function exists only for pedagogical purposes.
%
%   PARAMETERS:
%
%      X : a (H x W x D x N) tensor of N signals each of which has
%          height H, width W and D channels.
%
%      poolSize : a scalar filter dimension or a tuple of the form
%                 [filtHeight, filtWidth].
%
%      dzdy : (optional) the gradient from the tail of the CNN for a backward
%             pass computation; if empty, this function performs a
%             forward pass.
%
%   OPTIONS:
%       opts.stride : filter stride value (defaults to 1)
%
%       opts.St     : The transpose of S to use to implement average
%                     pooling.  If you plan to call avg_pool() many times
%                     for images of the same dimension, it is best
%                     to provide this so S need not be constructed
%                     from scratch each time.
%
%       opts.pad    : amount of zero padding to add to all sides 
%                     of the image.  Can be a scalar or an array
%                     [TOP BOTTOM LEFT RIGHT]
%
%       opts.asSingle :  set to true to force single-valued output
% 
%   NOTES:
%     In all cases where a sparse matrix vector multiply is called for
%     this code works with the transpose of S.  This is because 
%     the sequence of inner products comprising the multiplication 
%     is more efficient if data can be loaded into the CPU cache 
%     on a per-row basis.  However, Matlab's default is column-major;
%     the remedy is to work with S.' and realize that Matlab is 
%     smart enough to recognize St.' * x as a special form (it does
%     not do the naive thing of transposing St and then multiplying).

%
% mjp, march 2016

% Default options.
%
% The inputParser is not super fast; since this function will be
% called many times this crude but fast approach to options.
defaults.stride = 1;
defaults.St = [];
defaults.pad = [0 0 0 0];
defaults.asSingle = false;


% Special case: calling this function with no arguments creates a
% default options structure.
if nargin < 1
    Y = defaults;
    return;
end

if nargin < 4
    opts = defaults;
end

if nargin < 3 || ~isnumeric(dzdy), 
    dzdy = []; 
end

% Expand out scalar options
if numel(poolSize) == 1, 
    poolSize = [poolSize  poolSize]; 
end

if numel(opts.pad) == 1
    opts.pad = opts.pad * ones(1,4);
end


%------------------------------------------------------------
% Determine filter and image sizes
%------------------------------------------------------------
% TODO: this padding does not match matconvnet - figure this out!!
X = pad_image(X, opts.pad, false);


[hIn, wIn, d, n] = size(X);
assert(poolSize(1) < hIn  & poolSize(2) < wIn);


% If an S matrix is provded by caller, use it (we trust it has
% the correct dimensions).  Otherwise, create S.
if isempty(opts.St)
    [S, hOut, wOut] = s_avg_pool([hIn, wIn], poolSize, opts.stride);
    St = S.';
else
    [~, hOut, wOut] = s_avg_pool([hIn, wIn], poolSize, opts.stride, [], true);
    St = opts.St;
    assert(size(St,2) == hOut*wOut);
end


%----------------------------------------
% forward pass
%----------------------------------------
if isempty(dzdy)
    % Reshape the (h,w,d,n) data tensor into a matrix
    % and then compute the average pool as a matrix/matrix
    % product (which we then reshape back to tensor).
    %
    % The cast to a double is to accomodate matconvnet, which may send
    % us singles.
    %
    Y = St.' * double( reshape(X, hIn*wIn, d*n) );
    Y = reshape(Y, hOut, wOut, d, n);
    
%----------------------------------------
% backward pass
%----------------------------------------
else
    % For average pooling (see section 6.3 in matconvnet manual):
    %
    %   dzdy = S^T * vec(dzdy)
    %
    Y = St * double( reshape(dzdy, hOut*wOut, d*n) );
    Y = reshape(Y, hIn, wIn, d, n);
    Y = unpad_image(Y, opts.pad);
end


% cast output to single if input was a single or if 
% the caller requested it (this is to appease matconvnet)
if isa(X, 'single') || opts.asSingle
    Y = single(Y);
end
