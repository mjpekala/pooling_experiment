function compare_pooling_outputs(input, outputA, outputB, names)
if nargin < 4
    names = {'input', 'output A', 'output B'};
end

assert(ndims(input) == ndims(outputA))
assert(ndims(input) == ndims(outputB))
assert(ndims(input) <= 3);
d = size(input,3);

for ii=1:d
    figure('Position', [100 100 1200 300]); 
    subplot(1,4,1); imagesc(input(:,:,ii)); 
    title(sprintf('%s (channel %d)', names{1}, ii));
    
    subplot(1,4,2); imagesc(outputA(:,:,ii)); title(names{2});
    subplot(1,4,3); imagesc(outputB(:,:,ii)); title(names{3});
    subplot(1,4,4); imagesc(abs(outputA(:,:,ii) - outputB(:,:,ii)));  
    colorbar; title('difference');
end
