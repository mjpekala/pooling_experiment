function [Xout, Rout, St] = mf_pool(X, rMinMax, dzdy, opts)
%   PARAMETERS:
%
%      X : a (H x W x D x N) tensor of N signals each of which has
%          height H, width W and D channels.
%
%      rMinMax : The maximum function filter radii [rMin rMax]
%
%      dzdy : (optional) the gradient from the tail of the CNN for a backward
%             pass computation; if empty, this function performs a
%             forward pass.
%
%   OPTIONS:
%       opts.stride   : filter stride value (defaults to 1)
%
%       opts.St       : The transpose of S to use to implement average
%                       pooling.  If you plan to call this function many 
%                       times for inputs of the same dimension, it is best
%                       to re-use the filter matrices for efficiency.
%
%       opts.asSingle : set to true to force single-valued output

% mjp, March 2016
%

defaults.stride = 1;
defaults.St = [];
defaults.asSingle = false;

% Special case: calling this function with no arguments creates a
% default options structure.
if nargin < 1
    Xout = defaults;
    return;
end


if nargin < 4
    opts = defaults;
end

if nargin < 3, dzdy = []; end


rMin = floor(min(rMinMax));
rMax = floor(max(rMinMax));
assert((rMin < rMax) && (0 < rMin));
rVals = rMin:rMax;


X = double(X);        % in case matconvnet sent us singles
dzdy = double(dzdy);  %   "   "


% always pad input by maximum filter radius (so the filter can hit
% every pixel in the input image X)
%X = pad_image(X, rMax, false);
X = pad_image(X, rMax, true);
hIn = size(X,1);
wIn = size(X,2);

if isempty(opts.St) 
    % Create the array of average pooling filters
    %
    % XXX: this is a highly inefficient way to implement this
    %      operator due to all the redundant calculations 
    %      entailed by running multiple averaging filters.  An easy
    %      potential speedup is to use one small averaging filter and
    %      n-1 filters that add in the pixels of radius r+1 each time.
    %
    %      More sophisticated approaches are also worth considering.
    %
    fprintf('[%s]: creating pooling filters...\n', mfilename);
    St = cell(numel(rVals), 1);
    for ii = 1:numel(rVals), ri = rVals(ii);
        border = rMax - ri;
        [Sr, hOut, wOut] = s_avg_pool([hIn, wIn], [2*ri+1, 2*ri+1], opts.stride, border);
        St{ii} = Sr.';
    end
else
    % Using the filter bank provided by the caller.
    [~,hOut,wOut] = s_avg_pool([hIn, wIn], [2*rMax+1, 2*rMax+1], opts.stride, 0, true);
    St = opts.St;
end


%----------------------------------------
% Apply operator
%----------------------------------------

% Unlike average pooling, here the gradient depends upon which radius
% was selected at each point.  I'm not sure if there's an easier way
% to determine this without doing an forward pass to figure it
% out...so for the time being it looks as though we are stuck doing a
% forward pass no matter what.  


% Fast matrix tensor multiply (with and without transpose)
fast_mult_t = @(Mt, T) Mt.' * reshape(T, size(T,1)*size(T,2), size(T,3)*size(T,4));
fast_mult = @(M, T) M * reshape(T, size(T,1)*size(T,2), size(T,3)*size(T,4));

fwd_pass = @(Mt, T)  reshape(fast_mult_t(Mt, T), hOut, wOut, size(T,3), size(T,4));
bwd_pass = @(Mt, T)  reshape(fast_mult(Mt, T), hIn, wIn, size(T,3), size(T,4));


% Forward pass (apply all avg filters to input)
% Call to abs() is for the discrete maximal operator
% ******* TODO ********
%   Deal with absolute value
% ******* TODO ********
%Xfilt = cellfun(@(Mt) fwd_pass(Mt, abs(X)), St, 'UniformOutput', 0);
Xfilt = cellfun(@(Mt) fwd_pass(Mt, X), St, 'UniformOutput', 0);

dim = ndims(Xfilt{1}) + 1;   % this new dimension is for the # of averaging filters
Xfilt = cat(dim, Xfilt{:});

% apply definition of discrete maximal operator
[V,Ridx] = max(Xfilt, [], dim);
Rout = rVals(Ridx);  % map indices to radius values

if isempty(dzdy)
    % if this is a forward pass, we're done
    Xout = V;
else
    % the backward pass requires more work.
    Dfilt = cellfun(@(Mt) bwd_pass(Mt, dzdy), St, 'UniformOutput', 0);
    Dfilt = cat(dim, Dfilt{:});
    Dfilt = unpad_image(Dfilt, rMax);

    % adjust for stride by expanding rows and columns of Ridx.
    if opts.stride > 1
        er = repmat(1:size(Ridx,1), opts.stride, 1);  er = er(:);
        ec = repmat(1:size(Ridx,2), opts.stride, 1);  ec = ec(:)';
        Ridx = Ridx(er, :, :, :, :);
        Ridx = Ridx(:, ec, :, :, :);
    end
    
    Xout = tensor_slice(Dfilt, Ridx);
end

    
if isa(X, 'single') || opts.asSingle
    Xout = single(Xout);
end
