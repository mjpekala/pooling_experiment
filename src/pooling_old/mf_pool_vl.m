function [Xout, Rout] = mf_pool_vl(X, rMinMax, stride, dzdy)
% MF_POOL_VL  A version of maximal function pooling that uses
%             vl_nnpool under the hood.
%
%   PARAMETERS:
%
%      X : a (H x W x D x N) tensor of N signals each of which has
%          height H, width W and D channels.
%
%      rMinMax : The maximum function filter radii [rMin rMax]
%
%      dzdy : (optional) the gradient from the tail of the CNN for a backward
%             pass computation; if empty, this function performs a
%             forward pass.

% mjp, March 2016
%

% TODO: should "stride" mean something else in this context?
% We go through all the effort of determining a pooling size
% but then just go put everything back on the original grid.

if nargin < 4, dzdy = []; end
if nargin < 3 || isempty(stride), stride = 1; end

% Establish pooling radii
rMin = floor(rMinMax(1));
rMax = floor(rMinMax(2));
assert(rMin < rMax);
rVals = rMin:rMax;

% Unlike average pooling, here the gradient depends upon which radius
% was selected at each point.  I'm not sure if there's an easier way
% to determine this without doing an forward pass to figure it
% out...so for the time being it looks as though we are stuck doing a
% forward pass no matter what.  

% XXX: note: for now we do *not* take the abs() of X and rather assume
% that X arrives as a non-negative matrix.
%
% This can easily be relaxed; however in this case the gradient needs to change 
% slightly (introduce a sign(X) term).
assert(all(X(:) >= 0));  

% XXX: maybe cellfun is slowing us down?
% XXX: there are many repeated calculations in the implementation below that 
%      could be avoided.
%
Xfilt = cellfun(@(r) vl_nnpool(X, 2*r+1, 'stride', stride, 'pad', r, 'method', 'avg'), ...
                num2cell(rVals), 'UniformOutput', 0);
dim = ndims(Xfilt{1}) + 1;   % this new dimension is for the # of averaging filters
Xfilt = cat(dim, Xfilt{:});

% apply definition of discrete maximal operator
[V,Ridx] = max(Xfilt, [], dim);
Rout = rVals(Ridx);  % map indices to radius values


if isempty(dzdy)
    % forward pass = all done
    Xout = V;
else
    % backward pass
    Dfilt = cellfun(@(r) vl_nnpool(X, 2*r+1, dzdy, 'stride', stride, 'pad', r, 'method', 'avg'), ...
                    num2cell(rVals), 'UniformOutput', 0);
    Dfilt = cat(dim, Dfilt{:});
    
    % adjust for stride by expanding rows and columns of Ridx.
    if stride > 1
        % TODO: replace repmat
        er = repmat(1:size(Ridx,1), stride, 1);  er = er(:);
        ec = repmat(1:size(Ridx,2), stride, 1);  ec = ec(:)';
        Ridx = Ridx(er, :, :, :, :);
        Ridx = Ridx(:, ec, :, :, :);
    end
    
    Xout = tensor_slice(Dfilt, Ridx);
end
