function Xout = pad_image(X, pad, zeroPad)
% PAD_IMAGE  Adds zero padding to an image tensor.
%
%      X : a (H x W x D x N) tensor of N images each of which has
%          height H, width W and D channels.
%
%      pad : padding size array of the form [TOP BOTTOM LEFT RIGHT]
%            or a scalar pad that should be applied to all sides.

%
% mjp, march 2016

if nargin < 3, zeroPad = true; end

% Special case - no padding
if all(pad == 0)
    Xout = X;
    return;
end

if numel(pad) == 1
    pad = [pad pad pad pad];
end

%--------------------------------------------------
% do it
%--------------------------------------------------
top = pad(1);
bottom = pad(2);
left = pad(3);
right = pad(4);

Xout = zeros(size(X,1) + top + bottom, ...
             size(X,2) + left + right, ...
             size(X,3), size(X,4));

a = top+1;
b = left+1;
Xout(a:a+size(X,1)-1, b:b+size(X,2)-1, :, :) = X;


% If not zero padding, fill in with something else.
% For now, we mirror the edges.
% TODO: figure out what exactly matconvnet is doing and 
%       replicate that.
if ~zeroPad && top
    Xout(1:top,:) = flipud(Xout(top+1:2*top,:));
end
if ~zeroPad && bottom
    Xout(end-bottom+1:end,:) = flipud(Xout(end-2*bottom+1:end-bottom,:));
end
if ~zeroPad && left
    Xout(:,1:left) = fliplr(Xout(:,left+1:2*left));
end
if ~zeroPad && right
    Xout(:,end-right+1:end) = fliplr(Xout(:,end-2*right+1:end-right));
end

