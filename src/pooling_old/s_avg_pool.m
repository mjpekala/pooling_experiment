function [S, hOut, wOut] = s_avg_pool(szIn, szFilt, stride, border, dimsOnly)
% S_AVG_POOL  Creates a sparse matrix S that implements average pooling.
%
%   Parameters:
%      szIn     : a tuple of the form (H,W) specifying the 
%                 height and width of the input image.
%      szFilt   : a tuple of the form (A,B) specifying the 
%                 height and width of the average pooling filter.
%      stride   : the filter stride value
%      border   : number of border pixels to skip over [TOP BOTTOM LEFT RIGHT]
%      dimsOnly : if true, this function does not construct S but
%                 only returns the output image dimensions.
%       

% mjp, march 2016
%

if nargin < 5, dimsOnly=false; end

% deal with borders
if nargin < 4 || isempty(border)
    border = [0 0 0 0];
elseif numel(border) == 1
    border = border * [1 1 1 1];
end

b.top = border(1);
b.bottom = border(2);
b.left = border(3);
b.right = border(4);


% stride must be an integer > 0
if nargin < 3 || stride <= 0, stride = 1; end
stride = floor(stride);

% size of input signal and filter
hIn = szIn(1);      wIn = szIn(2);
hFilt = szFilt(1);  wFilt = szFilt(2);
filterWeight = 1 / prod(szFilt);

% Define the domain where the filter will be applied.
% This ensures the filter will be entirely supported by the image
% (excluding the border, if any).
hStart = 1 + b.top;
wStart = 1 + b.left;
hStop = hIn - (hFilt - 1) - b.bottom;
wStop = wIn - (wFilt - 1) - b.right;

% size of the output signal
hOut = ceil((hStop - hStart + 1) / stride);
wOut = ceil((wStop - wStart + 1) / stride);

% Special case: only asked to report output dimensions
if dimsOnly
    S = [];
    return
end


%--------------------------------------------------
% create S matrix
%--------------------------------------------------

% To create S, we construct dense vectors:
%   I := row indices of nonzeros in S
%   J := column indices of nonzeros in S
%   V := values of nonzeros in S
% and pass these to sparse().
%
% One could construct I, J with a nested for loop.
% However, the structure of S is such that these
% can be determined more efficiently.
%
% In fact, with the implementation below, >90% of the
% time spent in this function is the call to sparse()
% (which is just about the best we could hope for).
%
%------------------------------
% Create I
%------------------------------

% Each row in S has wFilt*hFilt nonzeros.  
% So I is just the number of rows of S replicated wFilt*hFilt times.
% Avoid repmat because it is slow.
%
nNonzero = (wOut*hOut)*(wFilt*hFilt);  % total number of nonzero elts in S
nRowsInS = 1:wOut*hOut;                 
I = reshape(nRowsInS(ones(wFilt*hFilt,1),:), nNonzero, 1);

%------------------------------
% Create J
%------------------------------

% Choosing columns in S is a little trickier - S is almost a
% banded matrix but not quite.
%

%                                          don't offset first column
%                                                     vvv
locations = bsxfun(@plus, hIn*(wStart:stride:wStop) - hIn, (hStart:stride:hStop)');
locations = locations(:);

filterOffsets = bsxfun(@plus, hIn*(0:wFilt-1), (0:hFilt-1)');
filterOffsets = filterOffsets(:);

J = bsxfun(@plus, filterOffsets(:), locations(:)');
J = J(:);

%------------------------------
% Create V
%------------------------------
V = ones(size(I)) * filterWeight;

%------------------------------
% Create S
%------------------------------
S = sparse(I, J, V);

%------------------------------
% it is possible the filter does not hit every pixel in the input
% image.  In this case, add a dummy value of 0 just to make sure S
% is the proper size.
%------------------------------
if size(S,2) < prod(szIn)
    S(wOut*hOut, wIn*hIn) = 0;
end
