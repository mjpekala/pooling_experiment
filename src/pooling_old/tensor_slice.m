function out = tensor_slice(A, B)
% TENSOR_SLICE  Uses tensor B to slice the last dimension of tensor A.
%
%   PARAMETERS:
%      A : a tensor with n dimensions
%      B : a tensor with n-1 dimensions; these must be the same as the first n-1 of A.
%          Each element in B is in 1:size(A,end), i.e. its values can be interpreted as
%          indices into the last dimension of A.


szA = size(A);
szB = size(B);

assert(ndims(A) == ndims(B)+1);
assert(all(szA(1:end-1) == szB));
assert(min(B(:)) >= 1);
assert(max(B(:)) <= szA(end));

% Reshape A so that B now indexes its columns
A = reshape(A, prod(szA(1:end-1)), szA(end));

% Slice and reshape.
ind = sub2ind(size(A), (1:size(A,1))', B(:));
out = reshape(A(ind), szA(1:end-1));
