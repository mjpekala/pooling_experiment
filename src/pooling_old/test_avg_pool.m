% Unit tests for the avg_pool() function.
%

% mjp, march 2016

Img = double(imread('cameraman.tif'));
Img = imresize(Img, [100 100]);

nTrials = 1e4;        % number of trials to run (for timing tests)
d = 3;                % number of dimensions of input signal
szFilt = 3;           % size of average pooling filter

%-------------------------------------------------------------------------------
% simulate a signal with d dimensions
%-------------------------------------------------------------------------------
X = zeros(size(Img,1), size(Img,2), d);
for ii=1:d
    X(:,:,ii) = Img + randn(size(Img))*(ii-1)*30;
end

fprintf('[%s] X has size: %s\n', mfilename, num2str(size(X)));
fprintf('[%s] Will run %d iterations of each implementation (for timing purposes)\n', mfilename, nTrials);


%===============================================================================
% Forward pass
%===============================================================================
fprintf('[%s] Testing forward pass...\n', mfilename);

% compare vl_nnpool() and avg_pool() for a set of strides and pads
opts = avg_pool();
opts.stride = 1;

for stride = 1:5
    opts.stride = stride;
    for jj = 1:4
        %opts.pad = 2 * (jj == [1 2 3 4]);
        opts.pad = 0; % TODO figure out what vl_nnpool is really doing for padding...
        Xvl = vl_nnpool(X, szFilt, 'method', 'avg', ...
                        'stride', opts.stride, 'pad', opts.pad);
        Xap = avg_pool(X, szFilt, [], opts);
        
        err = max(abs(Xvl(:) - Xap(:)));
        assert(err < 1e-6);
    end
end


% timing trial
opts.stride = 1;
opts.pad = 0;

tic
for ii = 1:nTrials
    Xvl = vl_nnpool(X, szFilt, 'method', 'avg', 'stride', opts.stride);
end
t1 = toc;

tic
for ii = 1:nTrials
    if ii == 1
        [Xap, St] = avg_pool(X, szFilt, [], opts);
        opts.St = St;
    else
        Xap = avg_pool(X, szFilt, [], opts);
    end
end
t2 = toc;


compare_pooling_outputs(X, Xvl, Xap, {'input', 'vl-nnpool (fwd)', 'avg-pool (fwd)'});

fprintf('[%s]: runtime for matconvnet:             %0.2f sec\n', mfilename, t1);
fprintf('[%s]: runtime for sparse matrix multiply: %0.2f sec\n', mfilename, t2);

err = max(abs(Xvl(:) - Xap(:)));
assert(err < 1e-6);
err


%===============================================================================
% Backward pass
%===============================================================================
fprintf('[%s] Testing backward pass...\n', mfilename);

for stride = 1:4
    Foo = vl_nnpool(X, szFilt, 'method', 'avg', 'stride', stride);
    
    for ii=1:20
        dzdy = 10*rand(size(Foo)); % just need a matrix that is of the correct size.

        Yvl = vl_nnpool(X, szFilt, dzdy, 'method', 'avg', 'stride', stride);
        
        opts = avg_pool();
        opts.stride = stride;
        Yap = avg_pool(X, szFilt, dzdy, opts);
        
        err = max(abs(Yvl(:) - Yap(:)));
        assert(err < 1e-6);
    end
end


% make sure when padding is added we at least get a matrix of the proper size...
opts = avg_pool();
for ii = 1:4
    opts.pad = 2 * (ii == [1 2 3 4]);
    
    Foo = vl_nnpool(X, szFilt, 'method', 'avg', 'pad', opts.pad);
    dzdy = 10*rand(size(Foo));
    
    Yvl = vl_nnpool(X, szFilt, dzdy, 'method', 'avg', 'pad', opts.pad);
    Yap = avg_pool(X, szFilt, dzdy, opts);
    
    assert(all(size(Yvl) == size(Yap)));
end

