% This is a quick test for the s_avg_pool() function.
%
%  pooling operation.

% mjp, march 2016


%-------------------------------------------------------------------------------
% Test out the border feature
%-------------------------------------------------------------------------------
fprintf('[%s]: testing border features\n', mfilename)

Z = zeros(100,100);
bs = 10;  % border size
Z(1:bs,:) = 1;
Z(end-bs+1:end,:) = 1;
Z(:,1:bs) = 1;
Z(:,end-bs+1:end) = 1;

% this should exactly omit the border of nonzeros in Z
S = s_avg_pool(size(Z), [3,3], 1, bs);
Zp = S * Z(:);
assert(all(Zp == 0));
assert(numel(Zp) == (80-2)^2);

% these should hit some of the ones
for ii = 1:4
    border = bs * [1 1 1 1];   
    border(ii) = bs-1;
    S = s_avg_pool(size(Z), [3,3], 1, border);
    Zp = S * Z(:);
    assert(~all(Zp == 0));
end


%-------------------------------------------------------------------------------
%  Test that multiplying a signal by S is equivalent to using
%  matconvnet's average pooling.
%-------------------------------------------------------------------------------
fprintf('[%s]: comparing vl_nnpool to multiplying by S\n', mfilename)
for stride = 1:4
    szFilt = [5,4];                         % filter size
    szImg =  [200,200];                     % image size
    X = double(imread('cameraman.tif')); 
    X = imresize(X, szImg);
    
    S = s_avg_pool(size(X), szFilt, stride);

    fprintf('[%s]: comparing operations for stride %d\n', mfilename, stride);
    X1 = vl_nnpool(X, szFilt, 'method', 'avg', 'stride', stride);
    X2 = reshape(S * X(:), size(X1,1), size(X1,2));

    compare_pooling_outputs(X, X1, X2, {'input', 'vl-nnpool', 'avg-pool'});
    suptitle(['stride: ', num2str(stride)]);

    assert(max(abs(X1(:) - X2(:)) < 1e-6));
end


%-------------------------------------------------------------------------------
% Just a quick speed test to see how much efficiency we are giving
% up by using sparse matrix multiplication in matlab vs whatever
% special sauces live in the bowels of matconvnet.
%
% Note that this result assumes S will be re-used (and hence only needs
% to be constructed once).
%-------------------------------------------------------------------------------
fprintf('[%s]: comparing efficiency to vl_nnpool ...please wait a few seconds...\n', mfilename);

nTrials = 1e4;
stride = 1;

tic;
for ii=1:nTrials
    Y1 = vl_nnpool(X, szFilt, 'method', 'avg', 'stride', stride);
end
t1 = toc;

tic;
S = s_avg_pool(size(X), szFilt, stride);
for ii=1:nTrials
    Y2 = reshape(S * X(:), size(Y1,1), size(Y1,2));
end
t2 = toc;

tic;
S = s_avg_pool(size(X), szFilt, stride);
St = S.';  % store by row to speed up matrix load into cache
for ii=1:nTrials
    Y3 = reshape(St.' * X(:), size(Y1,1), size(Y1,2));
end
t3 = toc;

fprintf('[%s]: runtime for matconvnet:    %0.2f sec\n', mfilename, t1);
fprintf('[%s]: runtime for smm:           %0.2f sec\n', mfilename, t2);
fprintf('[%s]: runtime for smm transpose: %0.2f sec\n', mfilename, t3);



%-------------------------------------------------------------------------------
% For testing speed of creating S
%-------------------------------------------------------------------------------
n = 20;
tic
for ii=1:n
    S = s_avg_pool([200,200], [10,10], 1);
end
fprintf('Time to instantiate %d matrices: %0.2f sec\n', n, toc);


