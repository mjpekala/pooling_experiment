% Unit tests for the mf_pool() function.
%

addpath('./thirdparty')

%-------------------------------------------------------------------------------
% Visualize action of maximum pooling filter (forward pass)
%-------------------------------------------------------------------------------
X = double(imread('cameraman.tif'));
X = cat(3, X, X, X);  % simulate multi-channel data
X = cat(4, X, X);     % simulate multi-example data

radii = [1, 10];      % radii to use for discrete maximum function


fprintf('[%s]: running multiple average filters\n', mfilename);
tic
Xr3 = avg_pool(X, [2*3+1, 2*3+1]);
Xr6 = avg_pool(X, [2*6+1, 2*6+1]);
Xr10 = avg_pool(X, [2*10+1, 2*10+1]);
toc


fprintf('[%s]: running maximum average\n', mfilename);
tic
%[Xmf,Rmf,St] = mf_pool(X, radii);
[Xmf, Rmf] = mf_pool_vl(X, radii);
toc
%opts = mf_pool();
%opts.St = St;


fprintf('[%s]: running maximum, second time\n', mfilename);
tic
mu = mean(X(:));
%[Xmf2,Rmf2] = mf_pool(X - mu, radii, [], opts);
[Xmf2,Rmf2] = mf_pool_vl(X - mu, radii);
Xmf2 = Xmf2 + mu;
toc


figure('Position', [100 100 1500 600]);
ha = tight_subplot(2,4, [.05 .05], .08, .08);
chan = 1;  % which channel to visualize

axes(ha(1));
imagesc(X(:,:,chan,1), [0 255]);  colorbar();
title('input image');

axes(ha(2));
imagesc(Xr3(:,:,chan,1), [0 255]); colorbar();
title(sprintf('average pooling, r=%d', 3));

axes(ha(3));
imagesc(Xr6(:,:,chan,1), [0 255]); colorbar();
title(sprintf('average pooling, r=%d', 6));

axes(ha(4));
imagesc(Xr10(:,:,chan,1), [0 255]); colorbar();
title(sprintf('average pooling, r=%d', 10));

axes(ha(5));
imagesc(Xmf(:,:,chan,1), [0 255]);  colorbar();
title(sprintf('centered mf pooling r \\in [%d %d]', radii(1), radii(2)));

axes(ha(6));
imagesc(Rmf(:,:,chan,1));  colorbar();
title('argmax r');

axes(ha(7));
imagesc(Xmf2(:,:,chan,1));  colorbar();
title(sprintf('centered mf pooling (mean image) r \\in [%d %d]', radii(1), radii(2)));

axes(ha(8));
imagesc(Rmf2(:,:,chan,1));  colorbar();
title('argmax r (mean image)');

saveas(gcf, 'mf-cameraman.fig');



%-------------------------------------------------------------------------------
% Backward pass.  
% This is mainly just to make sure code doesn't obviously crash (vs a test of
% actual correctness)
%-------------------------------------------------------------------------------
fprintf('[%s]: testing backward pass...\n', mfilename);

% assumes Xmf, opts created above
dzdyFake = 10*randn(size(Xmf));
tic
%[Y,R] = mf_pool(X, radii, dzdyFake, opts);
[Y,R] = mf_pool_vl(X, radii, 1, dzdyFake);
toc

assert(all(size(Y) == size(X)));
assert(all(R(:) == Rmf(:)));
