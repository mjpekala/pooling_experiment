% unit tests for pad_image()

%
% mjp, march 2016

Img = imread('cameraman.tif');

%--------------------------------------------------
% 2d image case
%--------------------------------------------------
Tmp = pad_image(Img, 0);
assert(all(all(Img == Tmp)));

n = 10;
Tmp = pad_image(Img, n);
assert(all(all(Img == Tmp(n+1:end-n, n+1:end-n))));

%--------------------------------------------------
% A full 4d tensor
%--------------------------------------------------
as_col = @(x) x(:);   % stupid matlab 
Img = randn(100,100,3,10);

Tmp = pad_image(Img, 0);
assert(all(Img(:) == Tmp(:)));

n = 10;
Tmp = pad_image(Img, [n 0 0 0]);
assert(all(as_col(Tmp(n,:,:,:) == 0)));
Tmp = Tmp(n+1:end,:,:,:);
assert(all(Img(:) == Tmp(:)));


%-------------------------------------------------------------------------------
% make sure we can remove padding
%-------------------------------------------------------------------------------
for ii = 1:100
    X = randn(100,100,3,10);
    pad = 10 * logical(rand(4,1) > .5);
    Xpadded = pad_image(X, pad);
    Xunpadded = unpad_image(Xpadded, pad);
    assert(all(X(:) == Xunpadded(:)));
end

