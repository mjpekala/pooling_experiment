

A = randn(100, 100);
B = randn(100, 100);

C = cat(3,A,B);

out = tensor_slice(C, ones(size(A)));
assert(all(out(:) == A(:)));

out = tensor_slice(C, 2*ones(size(A)));
assert(all(out(:) == B(:)));
